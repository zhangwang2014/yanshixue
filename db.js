var fs = require('fs');
var Sequelize = require('sequelize');
var config = require('./config.js');

var sequelize = new Sequelize(config.db.name, config.db.username, config.db.password, config.db.config);

var WordData = sequelize.define("wordData", {
    videoName: {type: Sequelize.STRING, comment: '视频名称'},
    labels: {type: Sequelize.JSON, comment: 'labels'},
    videoFeature: {type: Sequelize.STRING, comment: '视频特色'},
    prevWord: {type: Sequelize.STRING, comment: '前续'},
    nextWord: {type: Sequelize.STRING, comment: '后续'},
    testsWords:{type: Sequelize.JSON, comment: '测试的单词'},
    remarks:{type: Sequelize.STRING(65535), comment: '单词其它备注信息'},
    videoUrl:{type: Sequelize.STRING, comment: '视频url'},
    transVideoUrl:{type: Sequelize.STRING, comment: '压缩后视频url'},
    videoShortCut:{type: Sequelize.STRING, comment: '视频截图'},
    oriVideoUrl:{type: Sequelize.STRING, comment: '源视频url'},
    // 2016年10月11日新加
    wordQues:{type: Sequelize.JSON, comment: '测试的题目'}
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

var WordTests = sequelize.define("wordTests", {
    // testUniId: {type: Sequelize.STRING, comment: '测试题的唯一ID'},
    testWord: {type: Sequelize.STRING, comment: '测试单词'},
    testStem: {type: Sequelize.STRING(65535), comment: '题干'},
    testTitle: {type: Sequelize.STRING(65535), comment: '题目'},
    options: {type: Sequelize.JSON, comment: '选项及解析'},
    correctAnswer: {type: Sequelize.STRING, comment: '正确答案'},
    remarks:{type: Sequelize.STRING(65535), comment: '题目的备注信息'},
    firstTestTimes:{type: Sequelize.FLOAT, comment: '做过本题的人的数量'},
    firstPassNums:{type: Sequelize.FLOAT, comment: '第一次就正确的人数'},
    allTestTimes:{type: Sequelize.FLOAT, comment: '本题总共的测试次数（包括一人多次测试）'},
    allPassTimes:{type: Sequelize.FLOAT, comment: '总的通过测试的次数'},
    queType:{type: Sequelize.STRING, comment: '测试类型'},
    transContent:{type: Sequelize.STRING(65535), comment: '待翻译的句子'},
    transResult:{type: Sequelize.STRING(65535), comment: '翻译结果'}
},{
    freezeTableName: true // Model tableName will be the same as the model name
});

var AllData = sequelize.define("allData", {
    videoName: {type: Sequelize.STRING, comment: '视频名称'},
    type:{type: Sequelize.STRING, comment: '视频类别'},
    labels: {type: Sequelize.JSON, comment: 'labels'},
    videoLength:{type: Sequelize.STRING, comment: '视频时长'},
    viewingTimes:{type: Sequelize.INTEGER, comment: '视频时长'}
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

var UserInfo = sequelize.define("userInfo", {
    unionid: {type: Sequelize.STRING, comment: 'UserUnionid'},
    openid: {type: Sequelize.STRING, comment: 'UserOpenId'},
    nikeName: {type: Sequelize.STRING, comment: 'UserNikeName'},
    sex: {type: Sequelize.INTEGER, comment: "性别"},
    language: {type: Sequelize.STRING, comment: 'language'},
    city: {type: Sequelize.STRING, comment: 'city'},
    province: {type: Sequelize.STRING, comment: 'province'},
    country: {type: Sequelize.STRING, comment: 'country'},
    headimgurl: {type: Sequelize.STRING, comment: 'headimgurl'},
    privilege: {type: Sequelize.ARRAY(Sequelize.TEXT), comment: 'privilege'}
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

var UserData = sequelize.define("userData", {
    unionid: {type: Sequelize.STRING, comment: 'UserUnionid'},
    openid: {type: Sequelize.STRING, comment: 'UserOpenId'},
    unstudied: {type: Sequelize.JSON, comment: 'unstudied'},
    studied: {type: Sequelize.JSON, comment: 'studied'},
    deleted: {type: Sequelize.JSON, comment: 'deleted'},
    cumuTime: {type: Sequelize.INTEGER, comment: 'cumulativeTime'},
    cumuTimelistending: {type: Sequelize.INTEGER, comment: 'cumulativeTime-Listening'},
    cumuTimespeaking: {type: Sequelize.INTEGER, comment: 'cumulativeTime-Speaking'},
    cumuTimereading: {type: Sequelize.INTEGER, comment: 'cumulativeTime-Reading'},
    cumuTimewriting: {type: Sequelize.INTEGER, comment: 'cumulativeTime-Writing'},
    cumuTimeword: {type: Sequelize.INTEGER, comment: 'cumulativeTime-Word'},
    learningRecord:{type: Sequelize.JSON, comment: 'deleted'}
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

sequelize.sync();

module.exports = sequelize;
module.exports.WordData = WordData;
module.exports.AllData = AllData;
module.exports.UserInfo = UserInfo;
module.exports.UserData = UserData;
module.exports.WordTests = WordTests;