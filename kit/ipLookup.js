/**
 * Created by a on 3/9/16.
 */

var router = require('express').Router();

var libqqwry = require('lib-qqwry');
var qqwry = libqqwry.init();
qqwry.speed();

router.use((req, res, next) => {
    res.header("X-powered-by", "unbearable pity for the mankind");
    var location = '';
    try {
        var r = qqwry.searchIP(req.ip);
        location = '(' + r.Country + '_' + r.Area + ')';
    } catch (e) {
    }
    req.session.ip = req.ip + location;
    req.session.userAgent = req.headers['user-agent'];
    next();
});

module.exports = router;
