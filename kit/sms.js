/**
 * Created by a on 1/15/16.
 */
var router = require('express').Router();
var fetch = require('node-fetch');
var co = require('co');

var leanCloudId = 'RvYL0D4thEvBl7S1NELHcB51';
var leanCloudKey = 'xs9JgjnspFOgJ8YJfx4XpQvV';

router.post('/requestCode/:mobile', function (req, res, next) {
    //res.send();
    //return;
    if (/^1[0-9]{10}$/.test(req.params.mobile)) {
        var sendUrl = 'https://api.leancloud.cn/1.1/requestSmsCode';
        co(function*() {
            var reply = yield fetch(sendUrl,
                {
                    method: 'POST',
                    body: JSON.stringify({
                        mobilePhoneNumber: req.params.mobile
                    }),
                    headers: {
                        "X-LC-Id": leanCloudId,
                        "X-LC-Key": leanCloudKey,
                        "Content-Type": "application/json"
                    }
                });
            var json = yield reply.json();
            res.send(json);
        });
    } else {
        res.send();
    }
});

router.post('/verifyCode/:mobile/:code', function (req, res, next) {
    co(function*() {
        var verifyUrl = `https://api.leancloud.cn/1.1/verifySmsCode/${req.params.code}?mobilePhoneNumber=${req.params.mobile}`;
        var reply = yield fetch(verifyUrl,
            {
                method: 'POST',
                headers: {
                    "X-LC-Id": leanCloudId,
                    "X-LC-Key": leanCloudKey,
                    "Content-Type": "application/json"
                }
            });
        var data = yield reply.json();
        if (data.error) {
            res.send(data);
        } else {
            req.session.mobile = req.params.mobile;
            req.session.mobileVerified = true;
            req.session.name = req.body.name;
            req.session.email = req.body.email;
            res.json({
                status: 'success'
            });
        }
    });
});

module.exports = router;

