var router = require('express').Router();
var multer = require('multer');
var crypto = require('crypto');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'upload')
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            cb(err, err ? undefined : raw.toString('hex') + '_' + file.originalname);
        });
    }
});
var fileFilter = (req, file, cb) => {
    if (req.session.mobileVerified) {
        cb(null, true);
    } else {
        cb(null, false);
    }
}
var upload = multer({
    storage: storage,
    fileFilter: fileFilter,
    limits: {
        fileSize: 10 * 1024 * 1024
    }
});

router.post('/', upload.single('file'), function (req, res, next) {
    //TODO: better error handling when file size exceed, reply notification
    if (!req.session.files) {
        req.session.files = [];
    }
    if (req.file) {
        req.session.files.push(req.file);
        res.sendStatus(200);
    } else {
        res.status(500).send('register mobile no first');
    }
    //if (!req.session.files) {
    //    req.session.files = [];
    //}
});

module.exports = router;