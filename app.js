var path = require('path');
var fs = require('fs');
var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var session = require('express-session');

var oauth = require('./wechatOauth');

var sequelize = require('./db.js');
var UserInfo = sequelize.UserInfo;
var UserData = sequelize.UserData;
var WordData = sequelize.WordData;
var WordTests = sequelize.WordTests;

var SequelizeStore = require('connect-session-sequelize')(session.Store);
var sessionStore = new SequelizeStore({
    db: sequelize
});
var jsonParser = bodyParser.json();

app.use(session({
    secret: 'keyboard cat',
    store: sessionStore,
    resave: false,
    saveUninitialized: true,
    cookie: {maxAge: 365 * 24 * 3600 * 1000 * 10}
}));

//服务器把文件传输到本地
app.use('/dist', express.static(path.join(__dirname, 'dist')));
app.use('/admin', require('./routes/admin.js'));
app.use('/u', require('./routes/users.js'));
app.use('/userAction', require('./routes/userAction.js'));


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get('*', function (req, res, next) {
    // console.log(req.session);
    if (req.session.openid) {
        // console.log("该用户已存在");
        // console.log(req.session.openid);
        res.sendFile(path.resolve(__dirname, './dist/index.html')); //默认值
        // console.log("微信登录验证1");
        // next();
        // 存在则直接重定向
    } else if (req.query.openid) {
        req.session.openid = req.query.openid;
        // console.log("微信登录验证2");
        res.sendFile(path.resolve(__dirname, './dist/index.html')); //默认值
    } else {
        res.redirect(oauth.getUrl('http://daydayupinc.com'));
        // console.log("微信登录验证3");
        // console.log("重定向");
        // 测试内容
        // req.session.openid = "13579";

        // UserData.create({
        //     openid: "13579"
        // });

        // UserInfo.create({
        //     openid: "13579",
        //     nikeName: ".lcc"
        // });
        // res.sendFile(path.resolve(__dirname, './dist/index.html'));//默认值
    }
});


app.post('/data', jsonParser, function (req, res, err) {

    WordData.findAll().then(function (wordData) {
        // console.log(allData);
        var allWordData = [];
        
        for (var i = 0; i < wordData.length; i++) {
            allWordData[i] = {};
            allWordData[i].type = 'word';
            allWordData[i].title = wordData[i].videoName;
            allWordData[i].videoLength = '';

            allWordData[i].videoSrc = wordData[i].transVideoUrl;
            allWordData[i].shortCut = wordData[i].videoShortCut;
            var mainLabels = [];

            for (var k = 0; k < 3; k++) {
                // mainLabels[k] = wordData[i].labels[k];
                if (wordData[i].labels[k]) {
                    mainLabels.push(wordData[i].labels[k])
                }
            }

            allWordData[i].labels = mainLabels;
            var words = [];

            for (var k = 0; k < wordData[i].testsWords.length; k++) {
                var singleWord = [];
                singleWord[0] = wordData[i].testsWords[k].word;
                singleWord[1] = wordData[i].testsWords[k].explain;
                words.push(singleWord);
            }

            // allWordData[i].wordQues = wordData[i].wordQues;
            allWordData[i].wordQues = {};
            allWordData[i].wordQues.testUniId = wordData[i].wordQues;

            allWordData[i].words = words;
        }
        // console.log(allWordData);


        return allWordData;
        //    返回针对某个学生的单词数据
    }).then(function (allWordData) {
        var cardsData = allWordData;
        UserData.findOne({where: {openid: req.session.openid}}).then(function (userData) {

            var studiedCount = 0;

            if (userData.studied) {
                studiedCount = userData.studied.length;
                console.log("*****");
                console.log(studiedCount);
                console.log("*****");
                for (var i = 0; i < userData.studied.length; i++) {
                    var cardTitle = userData.studied[i].videoName;

                    for (var k = 0; k < tempData.length; k++) {
                        if (cardTitle == tempData[k].title) {
                            var a = tempData.splice(k, 1);
                            // console.log(a);
                            break;
                        }
                    }
                }
            }


            var sendData = true;
            var processNum = 0;

            if (cardsData.length > 0) {
                cardsData.map(function (singleData, index) {

                    WordTests.findById(singleData.wordQues.testUniId[0]).then(function (wordQue) {
                        var queData = {};
                        processNum++;
                        queData.testWord = wordQue.testWord;
                        queData.testUniId = wordQue.id;
                        if (wordQue.queType == "trans") {
                            queData.transContent = wordQue.transContent;
                            queData.transResult = wordQue.transResult;
                            queData.queType = wordQue.queType;
                        } else {
                            var tempTestStem = wordQue.testStem;
                            var frontStem = tempTestStem.slice(0, tempTestStem.indexOf(wordQue.testWord));
                            var endStem = tempTestStem.slice(tempTestStem.indexOf(wordQue.testWord) + wordQue.testWord.length);
                            queData.testStem = frontStem + "<span class='testWord'>" + wordQue.testWord + "</span>" + endStem;
                            queData.testTitle = wordQue.testTitle;
                            queData.options = wordQue.options;
                            queData.optClickState = [];
                            var correctAnswer = wordQue.correctAnswer.replace(/^\s+|\s+$/g, "");
                            wordQue.options.map(function (opt, index) {
                                if (opt.testOptContent) {
                                    var answer = opt.testOptContent.substr(0, 1);
                                }
                                // console.log(answer);
                                queData.options[index].testOptState = (answer == correctAnswer);
                            });
                        }
                        allWordData[index].wordQues = queData;
                        // console.log(queData);
                    }).then(function () {
                        if (sendData && processNum == cardsData.length) {
                            sendData = false;
                            res.send({
                                cardsData: cardsData,
                                studiedCount: studiedCount
                            });
                        }
                    })
                });
            } else {
                res.send({
                    cardsData: cardsData,
                    studiedCount: studiedCount
                });
            }

        });

    });

});

app.post('/obtUserData',jsonParser,function (req,res,err) {

    var userStudiedData = [];
    var resData = {};

    function compare(a, b) {
        if (a.studied.length > b.studied.length) {
            return -1;
        } else if (a.studied.length < b.studied.length) {
            return 1;
        } else if (a.studied.length == 0 && b.studied.length == 0) {
            return 0;
        } else {
            var alastLearningTime = a.studied[a.studied.length - 1].passTime;
            var blastLearningTime = b.studied[b.studied.length - 1].passTime;
            if (alastLearningTime > blastLearningTime) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    UserData.findAll().then(function (userData) {
        if (userData) {
            userData.map(function (singleData, index) {
                var singleRankingData = {};
                if(!singleData.studied){
                    singleData.studied=[];
                }
                singleRankingData.studied = singleData.studied;
                singleRankingData.openid = singleData.openid;
                userStudiedData.push(singleRankingData);
            })
        }
        userStudiedData.sort(compare);

        userStudiedData.map(function (userStudiedDataItem, index) {
            if (userStudiedDataItem.openid == req.session.openid) {
                resData.curRank = index + 1;
                console.log(resData.curRank);
            }
        });

        if(resData.curRank==1){
            resData.prevDis=null;
        }else if (resData.curRank > 1) {
            resData.prevDis = userStudiedData[resData.curRank - 2].studied.length - userStudiedData[resData.curRank - 1].studied.length;
        } else {
            resData.prevDis = 0;
        }
        resData.haveStudied = userStudiedData[resData.curRank - 1].studied.length;
        resData.studied = userStudiedData[resData.curRank - 1].studied;

        // res.send({ranking: userStudiedData});
    }).then(function () {
        UserInfo.findOne({where: {openid: req.session.openid}}).then(function (userInfo) {
            resData.headImg = userInfo.headimgurl;
            resData.nikeName = userInfo.nikeName;
            console.log(resData);
            res.send({resData: resData})
        })
    })
});


app.post('/learningProgress', jsonParser, function (req, res, err) {
    res.send('err');
});

app.listen(3456);