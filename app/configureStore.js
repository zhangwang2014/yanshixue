import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import cardsReducer from './FrontEnd/reducers/cardsReducer';
import searchReducer from './FrontEnd/reducers/searchReducer';
import userInfoReducer from './FrontEnd/reducers/userInfoReducer';

export const initialState = {
    cardsReducerState: {
        cardsDate: null,
        isFetching: false,
        cardsCount:5
    },
    searchReduceState: {
        searchBarFocused: false
    },
    userInfoReducerState: {
        headImgUrl:"",
        nikeName: "***",
        curRank: "?",
        curStudiedCount: "?",
        studied: [],
        isFetching: true,
        prevDis:"",
        studiedLoadCount:6,
        studiedFetching:false
    }
};

const comReducer = combineReducers({
    cardsReducerState: cardsReducer,
    searchReduceState: searchReducer,
    userInfoReducerState:userInfoReducer
});

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
// const store = createStore(comReducer,
//     composeEnhancers(
//         applyMiddleware(thunk)
//     ));

let store = createStore(
    comReducer,
    applyMiddleware(thunk)
);

export default store;