import React from 'react';
import ReactDOM from 'react-dom';
// import Root from './Root';
import store from './configureStore';
import {Provider} from 'react-redux';
import {Router, Route, IndexRoute, browserHistory} from 'react-router';
import ConCards from './FrontEnd/containers/conCards';
import KnowledgeStre from './FrontEnd/components/KnowledgeStre/KnowledgeStre';
import ConSearch from './FrontEnd/containers/ConSearch';
import ConUserInfo from './FrontEnd/containers/ConUserInfo';

import DashBoard from './BackStage/DashBoard/DashBoard';
import UpLoad from './BackStage/UpLoad/UpLoad';
import WordUpLoad from './BackStage/UpLoad/WordUpLoad/WordUpLoad';
import ReadingUpLoad from './BackStage/UpLoad/ReadingUpLoad/ReadingUpLoad';
import ListeningUpLoad from './BackStage/UpLoad/ListeningUpLoad/ListeningUpLoad';
import WritingUpLoad from './BackStage/UpLoad/WritingUpLoad/WritingUpLoad';
import SpeakingUpLoad from './BackStage/UpLoad/SpeakingUpLoad/SpeakingUpLoad';
import FileManage from './BackStage/FileManage/FileManage';
import App from './App';

require("babel-polyfill");

require('weui');
require('./index.scss');

class Notify extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Msg id="msg" type={this.props.params.type} title={this.props.params.title}
                    description={this.props.params.description} extraText="查看详情" extraHref="#"/>;
    }
}


const NotFound = ()=>(
    <div>
        Not Found
    </div>
);

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/">
                <Route path='/fe' component={App}>
                    <Route path="cards" component={ConCards}/>
                    <Route path="strengthen" component={KnowledgeStre}/>
                    <Route path="search" component={ConSearch}/>
                    <Route path="userInfo" component={ConUserInfo}/>
                </Route>
                <Route path="/admin" getComponent={(location, callback) => {
                    require.ensure([], function (require) {
                        var BackStage = require('./BackStage/BackStage');
                        callback(null, BackStage);
                    }, 'BackStage');
                }}>
                    <Route path="upload" component={UpLoad}>
                        <Route path="word" component={WordUpLoad}/>
                        <Route path="word/:name" component={WordUpLoad}/>
                        <Route path="reading" component={ReadingUpLoad}/>
                        <Route path="listening" component={ListeningUpLoad}/>
                        <Route path="speaking" component={SpeakingUpLoad}/>
                        <Route path="writing" component={WritingUpLoad}/>
                    </Route>
                    <Route path="filemanage" component={FileManage}/>
                    <Route path="dashBoard" component={DashBoard}/>
                </Route>
                <Route path="/notify/:type/:title/:description" component={Notify}/>
                <Route path="/custom/:course" component={NotFound}/>
                <Route path="*" component={NotFound}/>
            </Route>
        </Router>
    </Provider>,
    document.getElementById('root')
);

