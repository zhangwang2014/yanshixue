import React, {Component} from 'react';
import {Table, Icon, message, Select, Popconfirm} from 'antd';
import RefreshIcon from 'react-icons/lib/fa/refresh';
import ReactSelect from 'react-select';
import {request} from '../../FrontEnd/public/js/lib';
import {Link} from 'react-router';
const Option = Select.Option;

import 'react-select/dist/react-select.css';
import '../../public/css/antd.scss';
import './FileManage.scss';

class FileManage extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            dataSource: [],
            options: []
        });
        this.confirmDelete = this.confirmDelete.bind(this);
        this.cancelDelete = this.cancelDelete.bind(this);
    }

    componentDidMount() {
        request("/admin/readData/all").then((res)=>res.json()).then((res)=> {
            this.setState({
                dataSource: res.dataSource
            });
        })
    }

    confirmDelete(name) {
        request("/admin/deleteData/single", {videoName: name}).then((res)=>res.json()).then((res)=> {
            this.setState({
                dataSource: res.dataSource
            });
        });
        message.success('已删除');
    }

    cancelDelete() {
        message.error('取消删除');
    }

    logChange(val) {
        console.log("Selected: " + val);
    }

    selectChange(val) {
        console.log(val);
        request(`/admin/readData/${val}`).then((res)=>res.json()).then((res)=> {
            console.log(res.dataSource);
            console.log("123");
        })
    }

    render() {
        const pagination = {
            total: this.state.dataSource.length,
            showSizeChanger: false,
            onShowSizeChange(current, pageSize) {
                console.log('Current: ', current, '; PageSize: ', pageSize);
            },
            onChange(current) {
                console.log('Current: ', current);
            },
        };

        const columns = [{
            title: "编号",
            dataIndex: "index",
            key: 'index',
        }, {
            title: '名称',
            dataIndex: 'name',
            key: 'name',
        }, {
            title: '类别',
            dataIndex: 'category',
            key: 'category',
        }, {
            title: 'label1',
            dataIndex: 'label1',
            key: 'label1',
        }, {
            title: 'label2',
            dataIndex: 'label2',
            key: 'label2',
        }, {
            title: 'label3',
            dataIndex: 'label3',
            key: 'label3',
        }, {
            title: '视频时长',
            dataIndex: 'videoLength',
            key: 'videoLength',
        }, {
            title: '观看次数',
            dataIndex: 'viewingTimes',
            key: 'viewingTimes',
        }, {
            title: '操作',
            key: 'operation',
            render: (text, record) => (
                <span>
            <Popconfirm title="确认删除本项吗?" onConfirm={this.confirmDelete.bind(this, record.name)}
                        onCancel={this.cancelDelete}
                        okText="是"
                        cancelText="否">
                <a href="#">删除</a>
            </Popconfirm>
                <span className="ant-divider"></span>
            <Link to={`/admin/upload/word/${encodeURIComponent(record.name)}`}>修改</Link>
                <span className="ant-divider"></span>
            <a href="#" className="ant-dropdown-link">
                更多 <Icon type="down"/>
            </a>
        </span>
            ),
        }];

        return (
            <div className="fileManage content">
                <div className="manageTool">
                    <div className="select">
                        <Select defaultValue="all" style={{width: 120, height: 30}} onChange={this.selectChange}>
                            <Option value="all">all</Option>
                            <Option value="word">word</Option>
                            <Option value="reading" disabled>reading</Option>
                            <Option value="speaking" disabled>speaking</Option>
                            <Option value="listening" disabled>listening</Option>
                            <Option value="writing" disabled>writing</Option>
                        </Select>
                        <span className="upDate">
                            <RefreshIcon/>
                        </span>
                    </div>

                    <div className="reactSelect">
                        <ReactSelect
                            name="form-field-name"
                            value="one"
                            options={this.state.options}
                            onChange={this.logChange}
                        />
                    </div>
                </div>

                <div className="table">
                    <Table dataSource={this.state.dataSource} columns={columns} pagination={pagination}/>
                </div>

            </div>
        )
    }
}

export default FileManage;