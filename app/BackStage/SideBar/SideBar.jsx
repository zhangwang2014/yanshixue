import React, {Component} from 'react';
import DashBoardIcon from 'react-icons/lib/md/dashboard';
import FileUploadIcon from 'react-icons/lib/md/file-upload';
import TableIcon from 'react-icons/lib/fa/table';
import {Link, IndexLink} from 'react-router';

require('./SideBar.scss');

class SideBar extends Component {
    constructor(props) {
        super(props);
        this.state = ({})
    }

    render() {
        return (
            <div className="sideBar">
                <ul className="routes">
                    <li className="route"><Link to="/admin/dashboard" activeStyle={{color: 'white'}}
                                                style={{color: 'rgba(240,240,240,0.8)'}}><DashBoardIcon/></Link>
                    </li>
                    <li className="route"><Link to="/admin/upload/word" activeStyle={{color: 'white'}}
                                                style={{color: 'rgba(240,240,240,0.8)'}}><FileUploadIcon/></Link></li>
                    <li className="route"><Link to="/admin/filemanage" activeStyle={{color: 'white'}}
                                                style={{color: 'rgba(240,240,240,0.8)'}}><TableIcon/></Link></li>
                </ul>
            </div>
        )
    }
}

module.exports = SideBar;