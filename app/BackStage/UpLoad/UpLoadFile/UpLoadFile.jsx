import React, {Component} from 'react';
// import {request} from '../../lib.js';
import {Upload, message, Button, Icon} from 'antd';
require('./UpLoadFile.scss');

class UpLoadFile extends Component {
    constructor(props) {
        super(props);
        this.saveFileData = this.saveFileData.bind(this);
    }

    saveFileData(name, size) {
        var videoFormat = name.slice(name.lastIndexOf('.'), name.length);
        if (videoFormat == '.mp4') {
            var videoUrl = 'http://cdn.daydayupinc.com/' + this.props.type + '/' + encodeURI(name);
            var transVideoUrl = 'http://cdn.daydayupinc.com/' + 'trans/' + this.props.type + '/' + encodeURI(name);
            var videoShortCut = 'http://cdn.daydayupinc.com/' + 'shortcut/' + this.props.type + '/' + encodeURI(name);
            this.props.saveFileData('videoUrl', videoUrl);
            this.props.saveFileData('videoSize', (Math.round(size / 10000)) / 10);
            this.props.saveFileData('transVideoUrl', transVideoUrl);
            this.props.saveFileData('videoShortCut', videoShortCut);
        } else if (videoFormat == '.trec') {
            var oriVideoUrl = 'http://cdn.daydayupinc.com/' + "ori/" + this.props.type + '/' + encodeURI(name);
            this.props.saveFileData('oriVideoUrl', oriVideoUrl);
        }
    }

    render() {
        const props = {
            name: 'file',
            action: '/admin/upLoad/fileUpLoad',
            headers: {
                'Accept': 'application/json',
            },
            data: this.props.type ? { type: this.props.type } : {},
            onChange: (info) => {
                if (info.file.status !== 'uploading') {
                    console.log(info.file, info.fileList);
                }
                if (info.file.status === 'done') {
                    this.saveFileData(info.file.name, info.file.size);
                    message.success(`${info.file.name} 文件上传成功`);
                } else if (info.file.status === 'error') {
                    message.error(`${info.file.name} 文件上传失败`);
                }
            }
        };

        return (
            <div className="upLoadFile">
                <Upload {...props}>
                    <Button type="ghost">
                        <Icon type="upload"/> Click to Upload
                    </Button>
                </Upload>
            </div>
        )
    }
}

module.exports = UpLoadFile;