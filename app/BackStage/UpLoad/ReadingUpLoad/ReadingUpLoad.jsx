import React, {Component} from 'react';
import UpLoadFile from '../UpLoadFile/UpLoadFile';
import PlusIcon from 'react-icons/lib/fa/plus';
import SaveIcon from 'react-icons/lib/fa/floppy-o';
import FileIcon from 'react-icons/lib/fa/file';
import {message, Input} from 'antd';

require('./ReadingUpLoad.scss');

class ReadingUpLoad extends Component {
    constructor(props) {
        super(props);

        this.saveData = this.saveData.bind(this);
        this.newFile = this.newFile.bind(this);
    }

    saveData() {
        message.success('已保存');
    }

    newFile() {
        this.saveData();
    }

    render() {
        return (
            <div className="readingUpLoad">
                <div className="opeIcons">
                    <span className="icon saveData" onClick={this.saveData}>
                        <SaveIcon style={{height: '18px', width: '18px'}}/>
                    </span>
                    <span className="icon newFile" onClick={this.newFile}>
                        <FileIcon style={{height: '18px', width: '18px'}}/>
                    </span>
                </div>
                <div className="fileInfo">
                    <UpLoadFile/>
                    <ReadingInfo/>
                </div>
                <div className="testPart">
                    <ReadingTest/>
                </div>
            </div>
        );
    }
}

class ReadingInfo extends Component {

    render() {
        return (
            <div className="readingInfo">
                <div action="" className="basicInfo">
                    <h4>阅读基本信息</h4>
                    <div className="videoName">
                        <label htmlFor="">视频名称: </label>
                        <input type="text"/>
                    </div>
                    <div className="keyWords">
                        <label htmlFor="">key words: </label>
                        <input type="text"/>
                        <input type="text"/>
                        <input type="text"/>
                        <input type="text"/>
                    </div>
                    <div className="feature">
                        <label htmlFor="">视频特色: </label>
                        <input type="text"/>
                    </div>
                    <div className="category">
                        <label htmlFor="">分类: </label>
                        <input type="text"/>
                    </div>
                    <div className="chapter">
                        <label htmlFor="">章节: </label>
                        <input type="text"/>
                    </div>
                </div>

                <div className="otherInfo">
                    <h4>阅读备注信息</h4>
                    <div className="otherInfo-content">
                        <Input type="textarea" placeholder="在此添加备注信息，信息之间以'；'隔开" autosize={{minRows: 3}}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

class ReadingTest extends Component {


    render() {
        return (
            <div className="readingTest">
                <div className="icons">
                    <ul className="testList">
                        <li className="testItem">1</li>
                        <li className="testItem activeTest">2</li>
                        <li className="testItem">3</li>
                        <li className="testItem addNewTest icon"><PlusIcon/></li>
                    </ul>
                </div>

                <div className="testContent">
                    <div className="testStem">
                        <h4>题干</h4>
                        <textarea name="" id="" className="testStem-content">In a countercurrent exchange system, the blood vessels carrying cooled blood from the flippers run close enough to the blood vessels carrying warm blood from the body to pick up some heat from the warmer blood vessels; thus, the heat is transferred from the outgoing to the ingoing vessels before it reaches the flipper itself. This is the same arrangement found in an old-fashioned steam radiator, in which the coiled pipes pass heat back and forth as water courses through them. The leatherback is certainly not the only animal with such an arrangement; gulls have a countercurrent exchange in their legs. That is why a gull can stand on an ice floe without freezing.\nTPO-15: A Warm-Blooded Turtle\n\n\nWhy does the author mention old-fashioned steam radiator in the discussion of countercurrent exchange systems?</textarea>
                    </div>

                    <div className="testChoice">
                        <div className="plusIcon icon">
                            <PlusIcon/>
                        </div>
                        <h4>选项和解析</h4>
                        <ul className="choiceItems">
                            <li className="choiceItem">
                                <textarea name="" className="testChoice-content">A. To argue that a turtle's central heating system is not as highly evolved as that of other warmblooded animals</textarea>
                                <textarea name="" className="testChoice-analysis">解析</textarea>
                            </li>
                            <li className="choiceItem">
                                <textarea name="" className="testChoice-content">B. To provide a useful comparison with which to illustrate how a countercurrent exchange system works</textarea>
                                <textarea name="" className="testChoice-analysis">解析</textarea>
                            </li>
                            <li className="choiceItem">
                                <textarea name="" className="testChoice-content">C. To suggest that steam radiators were modeled after the sophisticated heating system of turtles</textarea>
                                <textarea name="" className="testChoice-analysis">解析</textarea>
                            </li>
                        </ul>
                    </div>

                    <div className="correctAnswer">
                        <h4>正确答案</h4>
                        <input type="text"/>
                    </div>
                </div>
            </div>
        )
    }
}

export default ReadingUpLoad;