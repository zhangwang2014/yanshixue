import React, {Component} from 'react';
import {Link, browserHistory} from 'react-router';

require('./UpLoad.scss');


class UpLoad extends Component {
    constructor(props) {
        super(props);
        this.state = ({});
    }
    render() {
        return (
            <div className="upload content">
                <ul className="nav">
                    <li className="navItem"><Link to="/admin/upload/word"
                                                  activeStyle={{color: 'rgba(0,0,0,1)', fontWeight: "bold"}}
                                                  style={{color: 'rgba(0,0,0,0.7)', textDecoration: 'none'}}>Word</Link>
                    </li>
                    <li className="navItem"><Link to="/admin/upload/reading"
                                                  activeStyle={{color: 'rgba(0,0,0,1)', fontWeight: "bold"}} style={{
                        color: 'rgba(0,0,0,0.7)',
                        textDecoration: 'none'
                    }}>Reading</Link></li>
                    <li className="navItem"><Link to="/admin/upload/listening"
                                                  activeStyle={{color: 'rgba(0,0,0,1)', fontWeight: "bold"}} style={{
                        color: 'rgba(0,0,0,0.7)',
                        textDecoration: 'none'
                    }}>Listening</Link></li>
                    <li className="navItem"><Link to="/admin/upload/speaking"
                                                  activeStyle={{color: 'rgba(0,0,0,1)', fontWeight: "bold"}} style={{
                        color: 'rgba(0,0,0,0.7)',
                        textDecoration: 'none'
                    }}>Speaking</Link></li>
                    <li className="navItem"><Link to="/admin/upload/writing"
                                                  activeStyle={{color: 'rgba(0,0,0,1)', fontWeight: "bold"}} style={{
                        color: 'rgba(0,0,0,0.7)',
                        textDecoration: 'none'
                    }}>Writing</Link></li>
                </ul>
                {this.props.children}
            </div>
        )
    }
}

export default UpLoad;