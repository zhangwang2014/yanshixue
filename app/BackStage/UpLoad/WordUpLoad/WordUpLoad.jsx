import React, {Component} from 'react';
import UpLoadFile from '../UpLoadFile/UpLoadFile';
import PlusIcon from 'react-icons/lib/fa/plus';
import TransIcon from 'react-icons/lib/md/translate';

import {message, Input, Tooltip} from 'antd';
import SaveIcon from 'react-icons/lib/fa/floppy-o';
import FileIcon from 'react-icons/lib/fa/file';
import {request} from '../../public/js/lib';

import {browserHistory} from 'react-router';

require('./WordUpLoad.scss');

class WordUpLoad extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            // wordParaphrase: [],
            // keyValue: "123",
            wordData: {
                videoName: '',
                labels: [],
                videoFeature: '',
                prevWord: '',
                nextWord: '',
                testsWords: [],
                remarks: '',
                videoUrl: '',
                transVideoUrl: '',
                videoShortCut: '',
                oriVideoUrl: '',
                videoLength: '',
                videoSize: '',
                testData: [1]
            }
        });

        this.addWordParaphrase = this.addWordParaphrase.bind(this);
        this.saveWordDataTmp = this.saveWordDataTmp.bind(this);
        this.saveData = this.saveData.bind(this);
        this.newFile = this.newFile.bind(this);
        this.addLabel = this.addLabel.bind(this);
        this.testSelect = this.testSelect.bind(this);
        this.addTest = this.addTest.bind(this);
        this.addTrans = this.addTrans.bind(this);    }

    componentDidMount() {
        if (this.props.params.name) {
            // console.log(this.props.params.name);
            var body = {
                videoName: this.props.params.name
            };
            request('/admin/readData/singleData', body).then((res) => res.json()).then((res) => {
                console.log(res.oldWordData);
                this.setState({
                    wordData: res.oldWordData
                })
            });
        }
    }

    addWordParaphrase() {
        if (this.state.wordData.testsWords.length < 8) {
            this.state.wordData.testsWords.push({});
            this.setState({});
            // console.log(this.state.wordData.testsWords);
        } else {
            message.warning('测试单词个数不能超过8个');
        }
    }

    addLabel() {
        this.state.wordData.labels.push("");
        this.setState({});
    }

    // TODO 学习redux的状态机制，用redux来管理状态
    saveWordDataTmp(cate, val) {
        this.state.wordData[cate] = val;
        // console.log(this.state.wordData);
        this.setState({});
    }

    saveData() {
        if (this.state.wordData.videoName) {
            request('/admin/saveData/saveWord', this.state.wordData).then((res) => res.json()).then((res) => {
                if (res.success) {
                    message.success('已保存');
                } else {
                    message.warning('保存失败');
                }
            });
        } else {
            message.warning("文件名不能为空");
        }
    }

    newFile() {
        // this.saveData();
        this.setState({
            wordData: {
                videoName: '',
                labels: [],
                videoFeature: '',
                prevWord: '',
                nextWord: '',
                testsWords: [],
                remarks: '',
                videoUrl: '',
                transVideoUrl: '',
                videoShortCut: '',
                oriVideoUrl: '',
                videoLength: '',
                videoSize: '',
                testData: [1]
            }
        });

        message.warning("清空文件");
    }

    testSelect(index) {
        this.state.wordData.testData[0] = index;
        this.setState({});
    }

    addTest() {
        this.state.wordData.testData.push({
            testWord: '',
            correctAnswer: '',
            testStem: '',
            testTitle: '',
            options: [],
            remarks: '',
            testUniId: null
        });

        //定位到当前题目
        this.state.wordData.testData[0] = this.state.wordData.testData.length - 1;
        this.setState({});
    }

    addTrans() {
        this.state.wordData.testData.push({
            testWord: '',
            transContent: '',
            transResult: '',
            testUniId: null,
            queType: "trans"
        });
        console.log('addTrans');
        this.state.wordData.testData[0] = this.state.wordData.testData.length - 1;
        this.setState({});
    }

    render() {
        return (
            <div className="wordUpLoad">

                <ul className="testList">
                    {this.state.wordData.testData.map((value, index) => {
                        if (index > 0) {
                            return (
                                <li className={(this.state.wordData.testData[0] == index) ? "testItem activeTest" : "testItem"}
                                    key={`test${index}`}>
                                    <Tooltip title={this.state.wordData.testData[index].testWord}>
                                        <span onClick={this.testSelect.bind(this, index) }>{index}</span>
                                    </Tooltip>
                                </li>
                            )
                        }
                    }) }
                </ul>

                <div className="opeIcons">
                    <Tooltip title="添加测试题">
                        <span className="icon addTest">
                            <TransIcon onClick={this.addTrans}/>
                        </span>
                    </Tooltip>

                    <Tooltip title="添加翻译题">
                        <span className="icon addTest">
                            <PlusIcon onClick={this.addTest}/>
                        </span>
                    </Tooltip>

                    <Tooltip title="保存数据">
                        <span className="icon saveData" onClick={this.saveData}>
                            <SaveIcon style={{height: '18px', width: '18px'}}/>
                        </span>
                    </Tooltip>

                    <Tooltip title="新建空白页">
                        <span className="icon newFile" onClick={this.newFile}>
                            <FileIcon style={{height: '18px', width: '18px'}}/>
                        </span>
                    </Tooltip>
                </div>

                <div className="fileInfoPart">
                    <UpLoadFile type="word" saveFileData={this.saveWordDataTmp}/>
                    <WordInfo addWordParaphrase={this.addWordParaphrase}
                              saveWordDataTmp={this.saveWordDataTmp}
                              wordData={this.state.wordData} addLabel={this.addLabel}/>
                </div>

                <div className="testPart">
                    <WordTestPart testData={this.state.wordData.testData} saveTestData={this.saveWordDataTmp}/>
                </div>
            </div>
        );
    }
}

class NewParaphrase extends Component {

    render() {
        return (
            <li>
                <span className="paraphrase-title prefix-title">{`单词${this.props.index + 1}`}</span>
                <input type="text" placeholder="单词"
                       onChange={this.props.saveTestWords.bind(this, this.props.index, true) }
                       value={this.props.valueWord}/>
                <span>: </span>
                <input type="text" className="paraphrase-content" placeholder="释义"
                       onChange={this.props.saveTestWords.bind(this, this.props.index, false) }
                       value={this.props.valueExplain}/>
            </li>
        )
    }
}

class WordInfo extends Component {
    constructor(props) {
        super(props);
        this.saveSingleData = this.saveSingleData.bind(this);
        this.saveLabels = this.saveLabels.bind(this);
        this.saveTestWords = this.saveTestWords.bind(this);
    }

    saveSingleData(cate, e) {
        var val = e.target.value;
        // console.log(e.target.value);
        this.props.saveWordDataTmp(cate, val);
    }

    saveLabels(index, e) {
        var labels = this.props.wordData.labels;
        // console.log(e.target.value);
        labels[index] = e.target.value;
        this.props.saveWordDataTmp('labels', labels);
    }

    saveTestWords(index, isWord, e) {
        var testWords = this.props.wordData.testsWords;
        if (isWord) {
            if (testWords[index]) {
                testWords[index].word = e.target.value;
            } else {
                testWords[index] = {};
                testWords[index].word = e.target.value;
            }
        } else {
            if (testWords[index]) {
                testWords[index].explain = e.target.value;
            } else {
                testWords[index] = {};
                testWords[index].explain = e.target.value;
            }
        }
        this.props.saveWordDataTmp('testsWords', testWords);
    }

    render() {
        return (
            <div className="wordInfo">
                <div className="basicInfo">
                    <h4>单词基本信息</h4>
                    <div className="videoName">
                        <span className="prefix-title">视频名称: </span>
                        <input type="text" onChange={this.saveSingleData.bind(this, 'videoName') }
                               value={this.props.wordData.videoName ? this.props.wordData.videoName : " "}/>
                    </div>
                    <div className="labels">
                        <span className="prefix-title">labels: </span>
                        <div className="labelList">
                            {this.props.wordData.labels.map((label, index) => {
                                return (<input type="text" onChange={this.saveLabels.bind(this, index) }
                                               key={`label${index}`} className="label"
                                               value={this.props.wordData.labels[index] ? this.props.wordData.labels[index] : " "}/>
                                )
                            }) }

                            <Tooltip title="添加标签">
                                <div className="plusIcon icon addLable" onClick={this.props.addLabel}>
                                    <PlusIcon/>
                                </div>
                            </Tooltip>
                        </div>
                    </div>
                    <div className="feature">
                        <span className="prefix-title">视频特色: </span>
                        <input type="text" onChange={this.saveSingleData.bind(this, 'videoFeature') }
                               value={this.props.wordData.videoFeature ? this.props.wordData.videoFeature : " "}/>
                    </div>
                    <div className="prevWord">
                        <span className="prefix-title">前续: </span>
                        <input type="text" onChange={this.saveSingleData.bind(this, 'prevWord') }
                               value={this.props.wordData.prevWord ? this.props.wordData.prevWord : " "}/>
                    </div>
                    <div className="nextWord">
                        <span className="prefix-title">后续: </span>
                        <input type="text" onChange={this.saveSingleData.bind(this, 'nextWord') }
                               value={this.props.wordData.nextWord ? this.props.wordData.nextWord : " "}/>
                    </div>
                </div>

                <div className="testInfo">
                    <h4>单词测试信息</h4>
                    <Tooltip title="添加释义单词">
                        <div className="plusIcon icon" onClick={this.props.addWordParaphrase}>
                            <PlusIcon/>
                        </div>
                    </Tooltip>
                    <ul>
                        <li className="paraphrase-example">
                            <span className="prefix-title">示例</span>
                            <input type="text" placeholder="单词"
                                   defaultValue="example"/>
                            <span>: </span>
                            <input type="text" className="paraphrase-content" placeholder="释义"
                                   defaultValue="n. 例子，榜样；vt. 作为…的例子，为…做出榜样；vi. 举例"/>
                        </li>
                        {this.props.wordData.testsWords.map((newParaphrase, index) => {
                            return <NewParaphrase key={`wordParaphrase${index}`} index={index}
                                                  saveTestWords={this.saveTestWords}
                                                  valueWord={this.props.wordData.testsWords[index].word ? this.props.wordData.testsWords[index].word : " "}
                                                  valueExplain={this.props.wordData.testsWords[index].explain ? this.props.wordData.testsWords[index].explain : " "}/>
                        }) }
                    </ul>
                </div>

                <div className="otherInfo">
                    <h4>单词备注信息</h4>
                    <div className="otherInfo-content">
                        <Input type="textarea" placeholder="在此添加备注信息，信息之间以'；'隔开" autosize={{minRows: 3}}
                               onChange={this.saveSingleData.bind(this, 'remarks') }
                               value={this.props.wordData.remarks ? this.props.wordData.remarks : " "}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

class WordTestPart extends Component {
    render() {
        return (
            <div className="wordTestPart">
                <h4>单词测试题</h4>

                {this.props.testData.map((test, index) => {
                    if (index > 0) {
                        return (
                            test.queType ? <TransQue oneTest={this.props.testData[index]}
                                                     index={index}
                                                     curIndex={this.props.testData[0]}
                                                     key={`testData${index}`}
                                                     testData={this.props.testData}
                                                     saveTestData={this.props.saveTestData}/> :
                                <WordTestQue oneTest={this.props.testData[index]} index={index}
                                             curIndex={this.props.testData[0]}
                                             key={`testData${index}`} testData={this.props.testData}
                                             saveTestData={this.props.saveTestData}/>
                        )
                    }
                }) }

            </div>
        )
    }
}

class WordTestQue extends Component {
    constructor(props) {
        super(props);
        this.saveSingleTest = this.saveSingleTest.bind(this);
        this.addQueOpt = this.addQueOpt.bind(this);
        this.saveSingleOpt = this.saveSingleOpt.bind(this);
    }

    saveSingleTest(index, queCate, e) {
        var testData = this.props.testData;
        var curQueData = testData[index];
        var curVal = e.target.value;
        curQueData[queCate] = e.target.value;
        testData[index] = curQueData;
        this.props.saveTestData("testData", testData);
    }

    addQueOpt(index) {
        var testData = this.props.testData;
        var curQueData = testData[index];
        if (!curQueData.options) {
            curQueData.options = []
        }
        curQueData.options.push({});
        console.log("addOpt");
        testData[index] = curQueData;
        this.props.saveTestData("testData", testData);
    }

    saveSingleOpt(index, optIndex, optCate, e) {
        var testData = this.props.testData;
        var curQueData = testData[index];
        var curVal = e.target.value;
        curQueData.options[optIndex][optCate] = curVal;
        testData[index] = curQueData;
        this.props.saveTestData("testData", testData);
    }

    render() {
        return (
            <div className={(this.props.index == this.props.curIndex) ? "wordTestQue" : "hidden"}>
                <div className="test-header">
                    <div className="testWord">
                        <span className="test-prefix">单词：</span>
                        <input placeholder="本题测试单词" value={this.props.oneTest.testWord}
                               onChange={this.saveSingleTest.bind(this, this.props.index, "testWord") }/>
                    </div>

                    <div className="testAnswer">
                        <span className="test-prefix">答案：</span>
                        <input placeholder="本题测试答案" value={this.props.oneTest.correctAnswer}
                               onChange={this.saveSingleTest.bind(this, this.props.index, "correctAnswer") }/>
                    </div>
                </div>

                <div className="testStem">
                    <span className="test-prefix">题干：</span>
                    <div className="testStem-content">
                        <Input type="textarea" placeholder="请输入测试题目题干" autosize={{minRows: 2}}
                               value={this.props.oneTest.testStem}
                               onChange={this.saveSingleTest.bind(this, this.props.index, "testStem") }/>
                    </div>
                    <span className="test-prefix">题目：</span>
                    <div className="testStem-title">
                        <Input type="textarea" placeholder="请输入测试题目题干" autosize
                               value={this.props.oneTest.testTitle}
                               onChange={this.saveSingleTest.bind(this, this.props.index, "testTitle") }/>
                    </div>
                </div>

                <div className="testOptions">
                    <span className="test-prefix">选项和解析：</span>
                    <Tooltip title="添加选项">
                        <div className="plusIcon icon addLable">
                            <PlusIcon onClick={this.addQueOpt.bind(this, this.props.index) }/>
                        </div>
                    </Tooltip>
                    {this.props.oneTest.options.map((opt, index) => {
                        return (
                            <TestOpt option={this.props.oneTest.options[index]} key={`oneTest${index}`}
                                     saveSingleOpt={this.saveSingleOpt} optIndex={index} index={this.props.index}/>
                        )
                    }) }
                </div>

                <div className="testRemark">
                    <span className="test-prefix">备注：</span>
                    <div className="testRemark-contnet">
                        <Input placeholder="备注内容" value={this.props.oneTest.remarks}
                               onChange={this.saveSingleTest.bind(this, this.props.index, "remarks") }/>
                    </div>
                </div>
            </div>
        )
    }
}

class TestOpt extends Component {

    render() {
        return (
            <div className="testOpt">
                <div className="testOpt-content">
                    <textarea placeholder="选项内容" value={this.props.option.testOptContent}
                              onChange={this.props.saveSingleOpt.bind(this, this.props.index, this.props.optIndex, "testOptContent") }/>
                </div>

                <div className="testOpt-explain">
                    <textarea placeholder="选项解析" value={this.props.option.testOptExplain}
                              onChange={this.props.saveSingleOpt.bind(this, this.props.index, this.props.optIndex, "testOptExplain") }/>
                </div>
            </div>
        )
    }
}

class TransQue extends Component {
    constructor(props) {
        super(props);
        this.saveSingleTest = this.saveSingleTest.bind(this);
    }

    saveSingleTest(index, queCate, e) {
        var testData = this.props.testData;
        var curQueData = testData[index];
        var curVal = e.target.value;
        curQueData[queCate] = e.target.value;
        testData[index] = curQueData;
        this.props.saveTestData("testData", testData);
    }

    render() {
        return (
            <div className={(this.props.index == this.props.curIndex) ? "transQue" : "hidden"}>
                <div className="testWord">
                    <span className="test-prefix">单词：</span>
                    <input placeholder="本题测试单词" value={this.props.oneTest.testWord}
                           onChange={this.saveSingleTest.bind(this, this.props.index, "testWord") }/>
                </div>

                <div className="transContent">
                    <span className="test-prefix">英文：</span>
                    <Input type="textarea" placeholder="请输入待翻译的句子" autosize={{minRows: 2}}
                           value={this.props.oneTest.transContent}
                           onChange={this.saveSingleTest.bind(this, this.props.index, "transContent") }/>
                </div>

                <div className="transResult">
                    <span className="test-prefix">中文：</span>
                    <Input type="textarea" placeholder="请输入翻译结果" autosize={{minRows: 2}}
                           value={this.props.oneTest.transResult}
                           onChange={this.saveSingleTest.bind(this, this.props.index, "transResult") }/>
                </div>

            </div>



        )
    }
}


export default WordUpLoad;