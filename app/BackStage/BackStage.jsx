import React, {Component} from 'react';
import SideBar from './SideBar/SideBar';

require('./BackStage.scss');

class BackStage extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="backStage" style={{width:"100%"}}>
                <SideBar/>
                {this.props.children}
            </div>
        )
    }
}

module.exports = BackStage;