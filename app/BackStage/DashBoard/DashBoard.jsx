import React, {Component} from 'react';
import {request} from '../public/js/lib';
require('./DashBoard.scss');

class DashBoard extends Component {
    constructor(props) {
        super(props);
        this.state=({
            allUserRankData:null
        });
        // this.allUserRankData = null;
    }

    componentDidMount() {
        request('/admin/dashBoard/rankAllUser').then((res)=>res.json()).then((res)=> {
            console.log(res.allUserRankData);
            this.setState({
                allUserRankData:res.allUserRankData
            });
        })
    }

    render() {
        return (
            <div className="dashBoard content">
                <ul className="rankList">
                    {
                        this.state.allUserRankData ?
                            this.state.allUserRankData.map((curUser, index)=> {
                                return (
                                    <li>
                                        <span className="rank rankItem">{index+1}</span>
                                        <span className="nikeName rankItem">{curUser.nikeName}</span>
                                        <div className="headImg rankItem"><img src={curUser.headImg} alt=""/></div>
                                        <span className="studiedCount rankItem">{curUser.studied.length}</span>
                                        <span className="corRate rankItem">{`${curUser.correctRate}%`}</span>
                                    </li>
                                )
                            }) : ""
                    }
                </ul>
            </div>
        )
    }
}

export default DashBoard;