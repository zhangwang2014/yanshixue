import {connect} from 'react-redux';
import UserInfo from '../components/UserInfo/UserInfo';
import {initialState} from '../../configureStore';
import {obtUserData,loadMoreStudiedCards} from '../actions/actUserInfo';

const mapStateToProps = (state=initialState)=> {

    return {
        headImgUrl:state.userInfoReducerState.headImgUrl,
        curRank:state.userInfoReducerState.curRank,
        prevDis:state.userInfoReducerState.prevDis,
        studied:state.userInfoReducerState.studied,
        nikeName:state.userInfoReducerState.nikeName,
        curStudiedCount:state.userInfoReducerState.curStudiedCount,
        studiedLoadCount:state.userInfoReducerState.studiedLoadCount,
        studiedFetching:state.userInfoReducerState.studiedFetching
    }
};

const mapDispatchToProps = (dispatch)=> {
    return {
        obtUserData:()=>{
            dispatch(obtUserData())
        },
        loadMoreStudiedCards:()=>{
            dispatch(loadMoreStudiedCards())
        }
    };
};

const ConUserInfo = connect(
    mapStateToProps,
    mapDispatchToProps,
)(UserInfo);

export default ConUserInfo;