import {connect} from 'react-redux';
import {searchBarFocus,searchBarBlur} from '../actions/actSearch';
import Search from '../components/Search/Search';
import {initialState} from '../../configureStore';

const mapStateToProps = (state=initialState)=> {
    return {
        searchBarFocused:state.searchReduceState.searchBarFocused
    }
};

const mapDispatchToProps = (dispatch)=> {
    return {
        searchBarFocus:()=>{
            dispatch(searchBarFocus())
        },
        searchBarBlur:()=>{
            dispatch(searchBarBlur())
        }
    }
};

const ConSearch = connect(
    mapStateToProps,
    mapDispatchToProps
)(Search);

export default ConSearch;