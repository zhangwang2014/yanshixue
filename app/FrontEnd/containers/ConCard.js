import {connect} from 'react-redux';
import Card from '../components/Cards/Card';
import {cardToggle,skipVideo,doQues,reviewWord,optClick,sectionOver,videoWatched,showTransResult} from '../actions/actCards';
import {initialState} from '../../configureStore';

const mapStateToProps = (state=initialState,ownProps)=> {
    return {
        cardType:ownProps.cardType,
        cardIndex:ownProps.cardIndex,
        showCard:state.cardsReducerState.cardsData[ownProps.cardIndex].showCard,
        videoName:ownProps.videoName,
        videoSrc:ownProps.videoSrc,
        shortCut:ownProps.shortCut,
        videoEnd:state.cardsReducerState.cardsData[ownProps.cardIndex].videoEnd,
        videoSkip:state.cardsReducerState.cardsData[ownProps.cardIndex].videoSkip,
        showWordsQues:state.cardsReducerState.cardsData[ownProps.cardIndex].showWordsQues,
        quesData:ownProps.quesData,
        reviewWordsData:ownProps.reviewWordsData,
        optClickState:state.cardsReducerState.cardsData[ownProps.cardIndex].wordQues.optClickState,
        wordsReviewState:state.cardsReducerState.cardsData[ownProps.cardIndex].wordsReviewState,
        testOver:state.cardsReducerState.cardsData[ownProps.cardIndex].testOver
    }
};

const mapDispatchToProps = (dispatch,ownProps)=> {
    return {
        toggleClick:()=>{
            dispatch(cardToggle(ownProps.cardIndex))
        },
        skipVideo:()=>{
            dispatch(skipVideo(ownProps.cardIndex))
        },
        doQues:()=>{
            dispatch(doQues(ownProps.cardIndex))
        },
        reviewWord:()=>{
            dispatch(reviewWord(ownProps.cardIndex))
        },
        optClick:(index,optIndex)=>{
            dispatch(optClick(index,optIndex))
        },
        sectionOver:()=>{
            dispatch(sectionOver(ownProps.cardIndex))
        },
        videoWatched:()=>{
            dispatch(videoWatched(ownProps.cardIndex))
        },
        showTransResult:()=>{
            dispatch(showTransResult(ownProps.cardIndex))
        }
    }
};

const ConCard = connect(
    mapStateToProps,
    mapDispatchToProps
)(Card);

export default ConCard;