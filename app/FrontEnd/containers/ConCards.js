import {connect} from 'react-redux';
import Cards from '../components/Cards/Cards';
import {obtCardsData,showMoreCards,showLoading} from '../actions/actCards';
import {initialState} from '../../configureStore';

const mapStateToProps = (state=initialState)=> {
    return {
        isFetching:state.cardsReducerState.isFetching,
        cardsData:state.cardsReducerState.cardsData,
        cardsCount:state.cardsReducerState.cardsCount
    }
};

const mapDispatchToProps = (dispatch)=> {
    return {
        obtCardsData:()=>{
            dispatch(obtCardsData())
        },
        showMoreCards:()=>{
            dispatch(showMoreCards())
        },
        showLoading:()=>{
            dispatch(showLoading())
        }
    };
};

//
const ConCards = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Cards);

export default ConCards;