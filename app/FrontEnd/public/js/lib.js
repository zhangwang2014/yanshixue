module.exports.request = (url, body)=> {
    return fetch(url, {
        credentials: 'same-origin',
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: body ? JSON.stringify(body) : undefined
    });
};

module.exports.getCookie = (name)=> {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
};

module.exports.log = function () {
    console.log.apply(console, arguments);
};

module.exports.arraySame = function (a, b) {
    return a.length == b.length && a.every(e=>b.indexOf(e) > -1);
};
