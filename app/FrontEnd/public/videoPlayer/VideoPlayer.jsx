require('./VideoPlayer.scss');
import React, {Component} from 'react';
var Swipeable = require('../js/Swipeable');

export default class VideoPlayer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            playing: false,
            progress: 0,
            time: 0,
            videoCanPlay: false
        };
        this.togglePlay=this.togglePlay.bind(this);
    }

    update() {
        // console.log('video player set state');
        var v = this.refs.video;
        this.setState({
            progress: v.currentTime / v.duration,
            time: v.currentTime
        });
        this.props.updateProgress && this.props.updateProgress(v.currentTime);
    };

    componentDidMount() {
        var v = this.refs.video;
        v.setAttribute('webkit-playsinline', true);
        //v.addEventListener("canplaythrough", ()=> {
        //    v.play();
        //    TODO: control progress?
        //});
        v.addEventListener('play', ()=> {
            this.progressFlag = setInterval(this.update.bind(this), 60);
        });

        v.addEventListener('pause', ()=> {
            clearInterval(this.progressFlag);
        });

        v.addEventListener('ended', ()=> {
            this.props.videoWatched();
        });


        v.addEventListener("canplay", ()=> {
            if(!this.unMounted){
                this.setState({
                    videoCanPlay: true
                });
            }
        });
    }

    componentWillUnmount() {
        // console.log('unmount video player');
        this.unMounted = true;
        clearInterval(this.progressFlag);
    }

    togglePlay() {
        if (this.state.playing) {
            this.setState({playing: false});
            this.refs.video.pause();
        } else {
            this.setState({playing: true});
            this.refs.video.play();
        }
    }

    play() {
        this.setState({playing: true});
        this.refs.video.play();
    }

    pause() {
        this.setState({playing: false});
        this.refs.video.pause();
    }

    //TODO: swiping right control

    swipedRight() {
        this.refs.video.currentTime = this.refs.video.currentTime + 5;
        this.update();
    }

    swipedLeft() {
        this.refs.video.currentTime = this.refs.video.currentTime - 5;
        this.update();
    }

    render() {

        return (
            <Swipeable
                onSwipedRight={this.swipedRight.bind(this)}
                onSwipedLeft={this.swipedLeft.bind(this)}
                onSwipedEmpty={this.togglePlay.bind(this)}
            >
                <div className="videoPlayer">
                    <img src={this.props.shortCut} className={this.state.videoCanPlay ? "cantPlay" : "canPlay"}/>
                    <video ref="video" width="100%" className={this.state.videoCanPlay ? "canPlay" : "cantPlay"}
                           preload="none" src={this.props.src}>
                    </video>
                    { !this.state.playing && <div className="control">
                        <div className="play i"></div>
                        <div className="pause i"></div>
                    </div> }
                    <div className={"progress-bar" + (this.state.playing ? "" : " stop")}
                         style={{width: this.state.progress * 100 + "%", background: this.props.color || 'black'}}>
                        <span className="current-time">
                            { (parseInt(this.state.time / 60) ? parseInt(this.state.time / 60) : "0") + ":" + parseInt(this.state.time % 60) }
                        </span>
                    </div>
                </div>
            </Swipeable>
        );
    }
}
