import {request} from '../public/js/lib';
import {initialState} from '../../configureStore';

function cardsReducer(state = initialState.cardsReducerState, action) {
    let cardsData = state.cardsData;

    var index = action.index;

    switch (action.type) {
        case "OBT_CARD_DATA":
            return Object.assign({}, state, {
                isFetching: true
            });

        case "OBT_CARD_DATA_SUCCESS":
            return Object.assign({}, state, {
                cardsData: action.payload,
                isFetching: false
            });

        case "SHOW_LOADING":
            return Object.assign({}, state, {
                isFetching: !state.isFetching
            });

        case "CARD_TOGGLE":
            // cardsDataTemp[action.index].showCard=!cardsDataTemp[action.index].showCard;
            return Object.assign({}, state, {
                cardsData: [...cardsData.slice(0, index),
                    Object.assign({},
                        cardsData[index],
                        cardsData[index].showCard = !cardsData[index].showCard),
                    ...cardsData.slice(index + 1)
                ]
            });

        case "SHOW_MORE_CARDS":
            console.log(state.cardsCount);
            return Object.assign({}, state, {
                cardsCount: state.cardsCount + 1
            });

        case "SKIP_VIDEO":
            console.log("123123");
            return Object.assign({}, state, {
                cardsData: [...cardsData.slice(0, index),
                    Object.assign({},
                        cardsData[index],
                        cardsData[index].videoSkip = true,
                        cardsData[index].videoEnd = true
                    ),
                    ...cardsData.slice(index + 1)
                ]
            });

        case "DO_QUES":
            console.log("DO_QUES");
            return Object.assign({}, state, {
                cardsData: [...cardsData.slice(0, index),
                    Object.assign({},
                        cardsData[index],
                        cardsData[index].showWordsQues = true,
                    ),
                    ...cardsData.slice(index + 1)
                ]
            });

        case "REVIEW_WORD":
            return Object.assign({}, state, {
                cardsData: [...cardsData.slice(0, index),
                    Object.assign({},
                        cardsData[index],
                        cardsData[index].wordsReviewState = true,
                    ),
                    ...cardsData.slice(index + 1)
                ]
            });

        case "OPT_CLICK":
            var optIndex = action.optIndex;
            var answerList = cardsData[index].wordQues.answerList;
            var curOpt = cardsData[index].wordQues.options[optIndex];

            return Object.assign({}, state, {
                cardsData: [...cardsData.slice(0, index),
                    Object.assign({},
                        cardsData[index],
                        cardsData[index].wordQues = Object.assign({},
                            cardsData[index].wordQues,
                            cardsData[index].wordQues.answerList = [...answerList, curOpt],
                            cardsData[index].wordQues.optClickState[optIndex] = true,
                            cardsData[index].testOver = cardsData[index].testOver ? cardsData[index].testOver : cardsData[index].wordQues.options[optIndex].testOptState
                        ),
                    ),
                    ...cardsData.slice(index + 1)
                ]
            });

        case "VIDEO_WATCHED":
            console.log("VIDEO_WATCHED");
            return Object.assign({}, state, {
                cardsData: [...cardsData.slice(0, index),
                    Object.assign({},
                        cardsData[index],
                        cardsData[index].videoEnd = true
                    ),
                    ...cardsData.slice(index + 1)
                ]
            });

        case "SHOW_TRANS_RESULT":
            console.log("SHOW_TRANS_RESULT");
            return Object.assign({}, state, {
                cardsData: [...cardsData.slice(0, index),
                    Object.assign({},
                        cardsData[index],
                        cardsData[index].testOver = true,
                        cardsData[index].wordQues = Object.assign({},
                            cardsData[index].wordQues,
                            cardsData[index].wordQues.showResult = true,
                        ),
                    ),
                    ...cardsData.slice(index + 1)
                ]
            });

        case "SECTION_OVER":
            //在前端删除该卡片
            console.log("SECTION_OVER");
            var body = {};
            var curTime = new Date;
            body.videoName = cardsData[index].title;
            body.testUniId = cardsData[index].wordQues.testUniId;
            body.curTime = curTime;
            body.answerList = cardsData[index].wordQues.answerList;
            body.videoSkip = cardsData[index].videoSkip;
            body.pass = cardsData[index].wordQues.answerList[0].testOptState;
            body.queType = cardsData[index].wordQues.queType;
            console.log(body);
            request('/userAction/sectionOver', body);
            return Object.assign({}, state, {
                cardsData: [...cardsData.slice(0, index),
                    ...cardsData.slice(index + 1)
                ]
            });

        default:
            return state
    }
}

export default cardsReducer;