// import {request} from '../public/js/lib';

import {initialState} from '../../configureStore';

function searchReducer(state = initialState.searchReduceState, action) {

    switch (action.type) {
        case "SEARCH_BAR_FOCUS":
            console.log("SEARCH_BAR_FOCUS");
            return Object.assign({}, state, {
                searchBarFocused: true
            });
        case "SEARCH_BAR_BLUR":
            console.log("SEARCH_BAR_BLUR");
            return Object.assign({}, state, {
                searchBarFocused: false
            });
        default:
            return state
    }
}

export default searchReducer;