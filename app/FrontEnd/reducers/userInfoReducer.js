import {request} from '../public/js/lib';
import {initialState} from '../../configureStore';

function userInfoReducer(state=initialState.userInfoReducerState,action) {

    switch (action.type){

        case "OBT_USER_Data":
            return Object.assign({},state,{
                isFetching:true
            });

        case "OBT_USER_DATA_SUCCESS":
            return Object.assign({},state,{
                headImgUrl:action.payload.headImg,
                curRank:action.payload.curRank,
                prevDis:state.prevDis,
                nikeName:action.payload.nikeName,
                studied:action.payload.studied,
                curStudiedCount:action.payload.haveStudied,
                isFetching: false,
            });

        case "LOAD_MORE_STUDIED_CARDS":
            return Object.assign({},state,{
                studiedFetching:true
            });

        case "LOAD_MORE_FINISHED":
            return Object.assign({},state,{
                studiedLoadCount:state.studiedLoadCount+5,
                studiedFetching:false
            });

        default:
            return state
    }
}

export default userInfoReducer;

