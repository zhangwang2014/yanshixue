import React from 'react';
import FaQuestionCircle from 'react-icons/lib/fa/question-circle.js';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import LinearProgress from 'material-ui/LinearProgress';
import Snackbar from 'material-ui/Snackbar';

import {deepOrange300} from 'material-ui/styles/colors';


require('./ScoresRecord.scss');

var directionStrTable = ['left-top', 'right-top', 'right-bottom', 'left-bottom'];

var colorTable = ['#297C9E', '#73C1B3', '#F06060', '#8A572E'];

var request = (url, body) => {
    return fetch(url, {
        credentials: 'same-origin',
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: body ? JSON.stringify(body) : undefined
    });
};

var MaterialScores = React.createClass({

    render(){
        return (
            <div className="material-cards">
                <MuiThemeProvider>
                    <ScoresRecord />
                </MuiThemeProvider>
            </div>
        )
    }
});

var Progress = React.createClass({

    render() {
        return (
            <div className="progress-bar">
                <div style={{width: `${this.props.value}%`}} className="progress"></div>
            </div>
        );
    }
});

var Chart = React.createClass({

    generateIndexMap() {
        let row = this.props.grid.row;
        let col = this.props.grid.col;
        let direct = this.props.direction;
        let count = row * col;
        let cursor = [
            [0, 0],
            [-1, 0]
        ];
        this._indexMap = [];
        let cursorIndex = 0;

        switch (direct) {
            case direct = "left-top":
                for (let i = 1; i <= count; i++) {
                    cursorIndex = Number(!cursorIndex);
                    let pos = cursor[cursorIndex];
                    if (cursorIndex == 1) {
                        if (pos[0] + 1 >= row) {
                            pos[0] = cursor[0][0] + 1;
                            pos[1]++;
                            cursor[0][0] = pos[0];
                            cursor[0][1] = pos[1];
                        } else {
                            pos[0]++;
                        }
                    } else {
                        if (pos[1] + 1 >= col) {
                            pos[0]++;
                            pos[1] = cursor[1][1] + 1;
                            cursor[1][0] = pos[0];
                            cursor[1][1] = pos[1];
                        } else {
                            pos[1]++;
                        }
                    }
                    this._indexMap[i] = pos[0] * col + pos[1];
                }
                break;

            case direct = "right-top":
                cursor[0][0] = 0;
                cursor[0][1] = col - 1;
                cursor[1][0] = -1;
                cursor[1][1] = col - 1;
                for (let i = 1; i <= count; i++) {
                    cursorIndex = Number(!cursorIndex);
                    let pos = cursor[cursorIndex];
                    if (cursorIndex == 1) {
                        if (pos[0] + 1 >= row) {
                            pos[0] = cursor[0][0] + 1;
                            pos[1]--;
                            cursor[0][0] = pos[0];
                            cursor[0][1] = pos[1];
                        } else {
                            pos[0]++;
                        }
                    } else {
                        if (pos[1] - 1 < 0) {
                            pos[0]++;
                            pos[1] = cursor[1][1] - 1;
                            cursor[1][0] = pos[0];
                            cursor[1][1] = pos[1];
                        } else {
                            pos[1]--;
                        }
                    }
                    this._indexMap[i] = pos[0] * col + pos[1];
                }
                break;

            case direct = "right-bottom":
                cursor[0][0] = row - 1;
                cursor[0][1] = 0;
                cursor[1][0] = row;
                cursor[1][1] = 0;
                for (let i = 1; i <= count; i++) {
                    cursorIndex = Number(!cursorIndex);
                    let pos = cursor[cursorIndex];
                    if (cursorIndex == 1) {
                        if (pos[0] - 1 < 0) {
                            pos[0] = cursor[0][0] - 1;
                            pos[1]++;
                            cursor[0][0] = pos[0];
                            cursor[0][1] = pos[1];
                        } else {
                            pos[0]--;
                        }
                    } else {
                        if (pos[1] + 1 >= col) {
                            pos[0]--;
                            pos[1] = cursor[1][1] + 1;
                            cursor[1][0] = pos[0];
                            cursor[1][1] = pos[1];
                        } else {
                            pos[1]++;
                        }
                    }
                    this._indexMap[i] = pos[0] * col + pos[1];
                }
                break;

            case direct = "left-bottom":
                cursor[0][0] = row - 1;
                cursor[0][1] = col - 1;
                cursor[1][0] = row;
                cursor[1][1] = col - 1;
                for (let i = 1; i <= count; i++) {
                    cursorIndex = Number(!cursorIndex);
                    let pos = cursor[cursorIndex];
                    if (cursorIndex == 1) {
                        if (pos[0] - 1 < 0) {
                            pos[0] = cursor[0][0] - 1;
                            pos[1]--;
                            cursor[0][0] = pos[0];
                            cursor[0][1] = pos[1];
                        } else {
                            pos[0]--;
                        }
                    } else {
                        if (pos[1] - 1 < 0) {
                            pos[0]--;
                            pos[1] = cursor[1][1] - 1;
                            cursor[1][0] = pos[0];
                            cursor[1][1] = pos[1];
                        } else {
                            pos[1]--;
                        }
                    }
                    this._indexMap[i] = pos[0] * col + pos[1];
                }
                break;
        }
    },

    renderItem(width, dWidth, color, index) {
        return (
            <span style={{width: width + '%'}} className="item" key={index}><b><i
                style={{width: dWidth + '%', backgroundColor: color}}></i></b></span>
        );
    },

    render() {
        let score = this.props.score;
        let count = this.props.grid.row * this.props.grid.col;
        let width = 100 / this.props.grid.col;
        let color = this.props.color;
        let map = [];

        this.generateIndexMap();

        for (let i = 1; i <= score; i++) {
            map[this._indexMap[i]] = 100;
        }
        if (score > Math.floor(score)) {
            map[this._indexMap[Math.floor(score) + 1]] = (score - Math.floor(score)) * 100;
        }

        return (
            <div className={`chart ${directionStrTable[this.props.direction]}`}>
                {
                    new Array(count).join(',').split(',').map((_, index) => {
                        return this.renderItem(width, map[index] || 0, color, index);
                    })
                }
            </div>
        );
    }

});

var ScoreCharts = React.createClass({

    renderChatItem(score, index) {
        return (
            <Chart
                key={index}
                score={Math.min(Math.max(0, score))}
                color={colorTable[index]}
                direction={directionStrTable[index]}
                grid={{row: 5, col: 6}}/>
        );
    },

    render() {
        return (
            <div className="scores-chart-wrapper">
                <div className="chart-wrapper left-top">
                    <h3 className="title">
                        <span className="text">阅读</span>
                        <span className="score">{(Math.round(this.props.readingScore*100))/100}
                            <span
                                className={this.props.showIncreased.reading ? "plus-score show" : "plus-score"}>{`+${this.props.sessionDelta[1]}`}</span>
                        </span>
                    </h3>
                    {this.renderChatItem(this.props.readingScore, 0)}
                </div>

                <div className="chart-wrapper right-top">
                    <h3 className="title">
                        <span className="score">
                             <span
                                 className={this.props.showIncreased.listening ? "plus-score show" : "plus-score"}>{ `+${this.props.sessionDelta[2]}` }</span>
                            {this.props.listeningScore}
                            </span>
                        <span className="text">听力</span>
                    </h3>
                    {this.renderChatItem(this.props.listeningScore, 1)}
                </div>

                <div className="chart-wrapper left-bottom">
                    {this.renderChatItem(this.props.speakingScore, 2)}
                    <h3 className="title">
                        <span className="text">口语</span>
                        <span className="score">{this.props.speakingScore}
                            <span
                                className={this.props.showIncreased.speaking ? "plus-score show" : "plus-score"}>{ `+${this.props.sessionDelta[3]}` }</span>
                        </span>
                    </h3>
                </div>

                <div className="chart-wrapper right-bottom">
                    {this.renderChatItem(this.props.writingScore, 3)}
                    <h3 className="title">
                        <span className="score">
                            <span
                                className={this.props.showIncreased.writing ? "plus-score show" : "plus-score"}>{`+${this.props.sessionDelta[4]}`}</span>
                            {this.props.writingScore}</span>
                        <span className="text">写作</span>
                    </h3>
                </div>
            </div>
        );
    }
});

// var TimeProgress = React.createClass({
//
//     render() {
//         return (
//             <div className="timeProgress">
//                 <span className="cuLearningTime" style={{
//                     width: (this.props.cuLearningTime / (120 * 60)) * 100 + '%',
//                     background: "rgb(12,113,13)"
//                 }}>
//                 </span>
//             </div>
//         );
//     }
//
//
// });

var ScoresRecord = React.createClass({

    getInitialState: function () {
        return ({
            readingScore: 0,
            listeningScore: 0,
            speakingScore: 0,
            writingScore: 0,
            wordScore: 0,
            cuLearningTime: 0,
            finalPlusScore: 0,
            sessionDelta: [0, 0, 0, 0, 0, 0, 0, 0],
            showTip: false,
            showIncreased: {},
            showUserName:false,
            userName:""
        })
    },


    componentDidMount() {
        let that = this;

        request('/learningProgress').then(res=>res.json()).then(res=> {

            var curDate = new Date();
            var curTime = curDate.getTime();
            var curDay = Math.floor(curTime / (24 * 60 * 60 * 1000));
            var curDayMSecond = curDay * (24 * 60 * 60 * 1000);
            var showIncreased = {};
            var sessionDelta = {};

            sessionDelta.todayLearningTime = 0;
            sessionDelta.reading = 0;
            sessionDelta.listening = 0;
            sessionDelta.speaking = 0;
            sessionDelta.writing = 0;
            sessionDelta.word = 0;
            sessionDelta.totalScore = 0;
            sessionDelta.finalScore = 0;

            this.setState({
                showUserName:true,
                userName:res.nikeName
            });

            if (res.deleteItems) {
                for (var i = 0; i < res.deleteItems.length; i++) {
                    // console.log(i + ":"+ res.deleteItems[i]);
                    if (res.deleteItems[i][1] > curDayMSecond) {
                        var curVideoLength = parseInt(res.deleteItems[i][2]);
                        //分别获取当天的单项时间以及累计时间
                        sessionDelta[res.deleteItems[i][3]] = sessionDelta[res.deleteItems[i][3]] + curVideoLength;
                        // console.log("类型");
                        // console.log(res.deleteItems[i][3]);
                        // console.log(sessionDelta[res.deleteItems[i][3]]);
                        sessionDelta.todayLearningTime = sessionDelta.todayLearningTime + curVideoLength;
                    }
                    // console.log("reading时长");
                    // console.log(sessionDelta.reading);
                }
                // console.log("各项累计");
                // console.log(sessionDelta);
            }

            // (Math.round(req.session.cumulativeTime['reading'] * 30 / req.session.totalTime['reading'] * 100)) / 100;
            sessionDelta.reading = (Math.round(sessionDelta.reading * 30 * 100 / res.totalTime.reading)) / 100;
            sessionDelta.word = (Math.round(sessionDelta.word * 100 / res.totalTime.word)) / 100;
            sessionDelta.totalScore = Math.round((sessionDelta.reading + sessionDelta.listening + sessionDelta.speaking + sessionDelta.writing) * 100) / 100;
            sessionDelta.finalScore = Math.round(((res.total * res.word) - ((-sessionDelta.totalScore + res.total) * ( -sessionDelta.word + res.word))) * 100) / 100;


            var typeId = 0;
            for (var type in sessionDelta) {
                typeId++;
                (function (type) {
                    setTimeout(function () {
                        showIncreased[type] = true;
                        console.log(showIncreased);
                        that.setState({
                            showIncreased: showIncreased
                        })
                    }, typeId * 300);
                })(type);
                // showIncreased[type] = true;
            }

            // console.log(sessionDelta);
            // console.log(sessionDelta.reading);
            // console.log(res.word);
            // console.log(sessionDelta.word);
            // console.log(showIncreased);

            this.setState({
                readingScore: res.reading - sessionDelta.reading,
                wordScore: Math.round((res.word - sessionDelta.word) * 100) / 100,
                cuLearningTime: res.allType - sessionDelta.todayLearningTime,
                sessionDelta: [sessionDelta.todayLearningTime, sessionDelta.reading, sessionDelta.listening, sessionDelta.speaking, sessionDelta.writing, sessionDelta.word, sessionDelta.finalScore],
            })
        });

        setTimeout(()=> {
            var showIncreased = this.state.showIncreased;
            for (var type in showIncreased) {
                // if (sessionDelta[type]) {
                //     showIncreased[type] = true;
                // }
                showIncreased[type] = false;
            }
            this.setState({
                showIncreased: showIncreased,
                readingScore: that.state.readingScore + that.state.sessionDelta[1],
                listeningScore: that.state.listeningScore + that.state.sessionDelta[2],
                speakingScore: that.state.speakingScore + that.state.sessionDelta[3],
                writingScore: that.state.writingScore + that.state.sessionDelta[4],
                wordScore: Math.round((that.state.wordScore + that.state.sessionDelta[5]) * 100) / 100,
                finalPlusScore: that.state.sessionDelta[6],
                cuLearningTime: this.state.cuLearningTime + this.state.sessionDelta[0],
                showUserName:false
                // sessionDelta: [0, 0, 0, 0, 0, 0],
            })
        }, 5500);
    },

    showTip: function () {
        this.state.showTip = !this.state.showTip;
        this.setState({})
    },

    render() {

        var styles = {
            RaisedButton: {
                position: "relative",
                left: "50%",
                transform: "translateX(-50%)",
                width: "90%",
                margin: "1.3rem 0"
            },
            labelStyle:{
                fontSize:"1rem"
            }
        };

        let total = this.state.listeningScore + this.state.readingScore + this.state.speakingScore + this.state.writingScore;

        return (
            <div className="row-m scrollable">
                <div className="today-progress-wrapper flex-box center">
                    {/*<div className="flex-col text">累计</div>*/}
                    <span className="todayLearningTime">
                        <span
                            className={this.state.showIncreased.todayLearningTime ? "plus-score show" : "plus-score"}>{`+${this.state.sessionDelta[0]}min`}</span></span>
                    <div className="flex-col-2">
                        <LinearProgress mode="determinate" value={this.state.cuLearningTime*100/(120*60)} color={deepOrange300}/>
                        {/*<TimeProgress cuLearningTime={this.state.cuLearningTime}*/}
                                      {/*todayLearningTime={this.state.todayLearningTime}/>*/}
                    </div>
                    {/*<div className="flex-col text">学习<b>{this.state.cuLearningTime}</b>min</div>*/}
                </div>

                {/*<div>*/}
                {/*<button className="start-btn"><a href="/learning">开始学习</a></button>*/}
                {/*</div>*/}

                <div className="start-btn">
                    <RaisedButton label="开始学习" style={styles.RaisedButton} labelStyle={styles.labelStyle} href="/learning"/>
                </div>

                <ScoreCharts
                    readingScore={this.state.readingScore}
                    listeningScore={this.state.listeningScore}
                    speakingScore={this.state.speakingScore}
                    writingScore={this.state.writingScore}
                    sessionDelta={this.state.sessionDelta}
                    showIncreased={this.state.showIncreased}
                />

                <div className="score-total">
                    {/*<h2 className="title">*/}
                    {/*单项总分:<span className="score">{total}<span*/}
                    {/*className={this.state.showIncreased.totalScore ? "plus-score show" : "plus-score"}>{`+${this.state.sessionDelta[6]}`}</span></span>*/}
                    {/*</h2>*/}

                    <Progress value={this.state.wordScore }/>
                    <div className="flex-box center comment">
                        <div className="flex-col text">单词覆盖率: {this.state.wordScore }%
                            <span
                                className={this.state.showIncreased.word ? "plus-score show" : "plus-score"}>{`+${this.state.sessionDelta[5]}%`}</span>

                        </div>

                        {/*<div className="flex-col link"><a href="#">我的单词图谱</a></div>*/}
                    </div>
                </div>

                <div className="final-score-total">
                    <h2 className="title">
                        <span className="text">最后得分:<span
                            className="score">{Math.round((total * this.state.wordScore ) * 100) / 100}<span
                            className={this.state.showIncreased.finalScore ? "plus-score show" : "plus-score"}>{`+${this.state.sessionDelta[6]}`}</span></span></span>

                        <button className="help-btn" ref="help-btn" onClick={this.showTip}>
                            <FaQuestionCircle className="iconfont"/>
                            {this.state.showTip ? <span className="tip show">最后得分等于单项得分乘以单词熟练度</span> :
                                <span className="tip">最后得分等于单项得分乘以单词熟练度</span>}
                        </button>
                    </h2>
                </div>
                <Snackbar open={this.state.showUserName} message={`欢迎您${this.state.userName}`}/>
            </div>
        )
    }

});

module.exports = MaterialScores;
// module.exports.TimeProgress = TimeProgress;