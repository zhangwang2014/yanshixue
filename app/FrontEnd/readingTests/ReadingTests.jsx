import React, {Component} from 'react';
import Checkbox from 'material-ui/Checkbox';
// import Snackbar from 'material-ui/Snackbar';
import RadioButtonChecked from 'material-ui/svg-icons/toggle/radio-button-checked';
import RadioButtonUnchecked from 'material-ui/svg-icons/toggle/radio-button-unchecked';
import CheckCircle from 'material-ui/svg-icons/action/check-circle';
// import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

require('./ReadingTests.scss');

var ReadingTests = React.createClass({

    getInitialState(){
        return ({
            answerCorrect: false,
            showArticle: true,
            testData: this.props.readingTests,
            curTest: 0,
            checkedState: [],
            checkTimes: 0,
            firstChecked: []
        })
    },

    answerCorrect(){
        this.state.answerCorrect = true;
        this.setState({})
    },

    answerReset(){
        this.state.answerCorrect = false;
        this.state.checkTimes = 0;
        this.state.checkedState = [];
        this.state.firstChecked = [];
        this.setState({});
    },

    checked(i){
        this.state.checkedState[i] = true;
        if (this.isMounted) {
            this.setState({});
        }
        console.log("checked");
        console.log(i);
    },

    whetherFirstChecked(i){
        if (this.state.checkTimes == 1) {
            this.state.firstChecked[i] = true;
        } else {
            this.state.firstChecked[i] = false;
        }
        this.setState({});
    },

    nextQuestion(){
        this.state.curTest = this.state.curTest + 1;
        this.answerReset();
        this.props.changeCardPosition();
        if (this.isMounted) {
            this.setState({});
        }
    },

    checkTimesCount(){
        this.state.checkTimes++;
        this.setState({});
    },

    sectionFinished(){
        this.props.sectionOver();
    },

    render(){
        return (
            <div className="readingTests">
                <ReadingArticle article={this.state.testData[this.state.curTest][0]}/>
                <ReadingTest answerCorrect={this.answerCorrect}
                             question={this.state.testData[this.state.curTest][1]}
                             answer={[this.state.testData[this.state.curTest][2],
                                 this.state.testData[this.state.curTest][3],
                                 this.state.testData[this.state.curTest][4],
                                 this.state.testData[this.state.curTest][5]]}
                             commonAnalysis={this.state.testData[this.state.curTest][6]}
                             analysis={[this.state.testData[this.state.curTest][7],
                                 this.state.testData[this.state.curTest][8],
                                 this.state.testData[this.state.curTest][9],
                                 this.state.testData[this.state.curTest][10]]}
                             correctAnswer={this.state.testData[this.state.curTest][11]}
                             checkedState={this.state.checkedState}
                             checked={this.checked} checkTimes={this.state.checkTimes}
                             checkTimesCount={this.checkTimesCount}
                             firstChecked={this.state.firstChecked}
                             whetherFirstChecked={this.whetherFirstChecked}
                />
                <div className={this.state.answerCorrect ? "nextTest" : "hidden"}>
                    {(this.state.curTest == (this.state.testData.length - 1)) ?
                        <FlatButton label="本节学习结束" onClick={this.sectionFinished}/> :
                        <FlatButton label="下一题" onClick={this.nextQuestion}/>}
                </div>
                {/*<div className="nextTest"><RaisedButton label="学好啦"/></div>*/}
            </div>
        )
    }

});

var ReadingArticle = React.createClass({


    render(){

        return (
            <div className="readingArticle">
                <p>PASSAGE EXCERPT:</p>
                <p className="article" dangerouslySetInnerHTML={{__html: this.props.article}}>

                </p>
                <p className="articleSource">
                    <span>TPO-3:</span>
                    Depletion of the Ogallala Aquifer
                </p>
            </div>

        )
    }

});

var ReadingTest = React.createClass({

    render(){

        var readingAnswers = [];

        for (var i = 0; i <= 3; i++) {
            var answerItem = <ReadingAnswer answer={this.props.answer[i]} answerCorrect={this.props.answerCorrect}
                                            analysis={this.props.analysis[i]} correctAnswer={this.props.correctAnswer}
                                            checkedState={this.props.checkedState[i]} checked={this.props.checked}
                                            id={i} commonAnalysis={this.props.commonAnalysis}
                                            checkTimes={this.props.checkTimes}
                                            checkTimesCount={this.props.checkTimesCount}
                                            firstChecked={this.props.firstChecked}
                                            whetherFirstChecked={this.props.whetherFirstChecked}
                                            key={this.props.answer[i]}/>;
            readingAnswers.push(answerItem);
        }

        return (
            <div className="readingTest">
                <ReadingQuestion question={this.props.question}/>
                {readingAnswers}
            </div>
        )
    }
});

var ReadingQuestion = React.createClass({

    render(){

        return (
            <div className="readingQuestion">
                <p className="question">
                    {this.props.question}
                </p>
            </div>
        )
    }

});

var ReadingAnswer = React.createClass({

    getInitialState(){
        return ({
            // checked: false,
            CheckedIcon: <RadioButtonChecked style={{fill: '#FF8A65'}}/>,
            // disabled: false
        })
    },

    onCheck(){
        this.props.checkTimesCount();
        var i = Number(this.props.id);
        this.props.checked(i);
        this.props.whetherFirstChecked(i);
        // if (this.props.checkTimes == 1) {
        //     this.props.firstChecked[i] = true
        // } else {
        //     this.props.firstChecked[i] = false
        // }
        // this.state.checked = (this.state.checked) ? false : true;
        // this.state.disabled = true;

        if (this.props.correctAnswer == this.props.answer) {
            this.state.CheckedIcon = <CheckCircle/>;
            this.props.answerCorrect()
        } else {
            this.state.CheckedIcon = <RadioButtonChecked style={{fill: '#FF8A65'}}/>
        }
        this.setState({})
    },


    render(){
        var styles = {
            checkBox: {
                width: "100%",
                display: "inline-block",
            },
            labelStyle: {
                fontSize: "0.7rem",
                lineHeight: 1.5,
                letterSpacing: "-.004rem",
                textAlign: "justify"
            },
        };

        return (
            <div className="readingAnswer">
                <Checkbox checkedIcon={this.state.CheckedIcon} uncheckedIcon={<RadioButtonUnchecked/>}
                          style={styles.checkBox} labelStyle={styles.labelStyle} onCheck={this.onCheck}
                          checked={this.props.checkedState}
                          label={this.props.answer} disabled={this.props.checkedState}/>

                <p className={this.props.checkedState ? "analysis" : "hidden"}>{(this.props.firstChecked[Number(this.props.id)]) ? this.props.commonAnalysis : ""}{this.props.analysis}</p>
                {/*<Snackbar message={this.props.explain} open={this.state.snackbarOpen}/>*/}
            </div>
        )
    }
});

module.exports = ReadingTests;