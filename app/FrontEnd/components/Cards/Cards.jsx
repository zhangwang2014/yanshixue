import React from 'react';
// import Card from './Card';
import ConCard from '../../containers/ConCard';
import LoadMore from '../LoadMore/LoadMore';

require('./Cards.scss');

class Cards extends React.Component {
    constructor(props) {
        super(props);
        this.cardsList = null;
        this.firstTimeReactBottom = true;
        this.timer = false;
        this.reachBottom = this.reachBottom.bind(this);
        this.pageScroll = this.pageScroll.bind(this);
    }

    componentDidMount() {
        const {obtCardsData} = this.props;
        this.cardsList.addEventListener('scroll', this.pageScroll);
        // obtCardsData();
        obtCardsData();
    }

    pageScroll() {
        if (this.props.cardsData.length >= this.props.cardsCount) {
            if (this.reachBottom()) {
                // console.log("abc");
                setTimeout(()=> {
                    this.timer = false;
                    this.firstTimeReactBottom = true;
                    console.log("showMoreCards");
                    this.props.showMoreCards();
                    this.props.showLoading();
                }, 1000);
            }
        }
    }

    reachBottom() {
        var reachBottom = (this.cardsList.scrollHeight - this.cardsList.scrollTop) <= (this.cardsList.clientHeight) + 1;
        if (reachBottom) {
            if (this.firstTimeReactBottom) {
                console.log("123123");
                this.firstTimeReactBottom = false;
                this.props.showLoading();
                return true;
            }
            if (this.timer) {
                return false;
            }
            this.timer = true;
        }
    }

    render() {
        const {isFetching, cardsData, cardsCount} = this.props;
        // console.log(cardsData);
        return (
            <div className="cards">
                <div className="cardList scrollable" ref={(cardsList)=> {
                    this.cardsList = cardsList
                }}>
                    {cardsData ? cardsData.map((cardData, index)=> {
                        if (index < cardsCount) {
                            return (<ConCard cardType={cardData.type} videoName={cardData.title}
                                             videoSrc={cardData.videoSrc} shortCut={cardData.shortCut}
                                             labels={cardData.labels} quesData={cardData.wordQues}
                                             reviewWordsData={cardData.words} cardIndex={index}
                                             key={`card${index}`}/>)
                        }
                    }) : ""}
                </div>
                <div className={isFetching ? "loadMore" : "hidden"}>
                    <LoadMore/>
                </div>
            </div>
        )
    }
}

export default Cards;