import React from 'react';
import ThumbUp from 'react-icons/lib/md/thumb-up';
import SkipBtn from 'react-icons/lib/md/skip-next';
import VideoPlayer from '../../public/videoPlayer/VideoPlayer';
import WordsQues from '../WordsTests/WordsQues/WordsQues';
import WordsReview from '../WordsTests/WordsReview/WordsReview';

class Card extends React.Component {

    render() {
        const {
            cardType, cardIndex, showCard, videoName, videoSrc, shortCut, videoEnd, quesData, showWordsQues,
            reviewWordsData, wordsReviewState, skipVideo, toggleClick, reviewWord, doQues, sectionOver, optClickState,
            optClick, testOver, videoWatched, showTransResult
        } = this.props;
        var addTest = {
            "word": showWordsQues ?
                <WordsQues sectionOver={sectionOver} quesData={quesData} optClickState={optClickState}
                           optClick={optClick} cardIndex={cardIndex} testOver={testOver}
                           showTransResult={showTransResult}/> :
                <WordsReview wordsReviewState={wordsReviewState} reviewWordsData={reviewWordsData}
                             doQues={doQues} reviewWord={reviewWord}/>,
            "reading": "",
            "listening": ""
        };

        return (
            <div className="card" ref="card">
                <div className="weui-panel__bd">
                    <div className="weui-media-box weui-media-box_text">
                        <h4 className="weui-media-box__videoName card-title"
                            onClick={toggleClick}>{videoName ? videoName : "..."}</h4>
                        <div className={showCard ? "hidden" : 'cardDes'}>
                            <p className="weui-media-box__desc"> <span className="keyWord">本节重点:</span>occur,before , _ced/_ceed/_cess=go ,
                                unprecedented</p>
                            {/*<ul className="weui-media-box__info">*/}
                                {/*<li className="weui-media-box__info__meta"><ThumbUp/></li>*/}
                                {/*<li className="weui-media-box__info__meta">237</li>*/}
                                {/*<li className="weui-media-box__info__meta weui-media-box__info__meta_extra">5min28s</li>*/}
                            {/*</ul>*/}
                        </div>

                        <div className={showCard ? "other-cards" : "hidden"}>
                            {videoEnd ? addTest[cardType] : <div className="card-video">
                                <VideoPlayer src={videoSrc} shortCut={shortCut} videoWatched={videoWatched}/>
                                <div className="videoSkip-btn">
                                    <SkipBtn onClick={skipVideo}/>
                                </div>
                            </div>}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Card;