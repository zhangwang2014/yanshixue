import React from 'react';
require('./UserInfo.scss');
import StudiedRecord from './StudiedRecord/StudiedRecord';
import Chart from 'chart.js';

class UserInfo extends React.Component {
    constructor(props) {
        super(props);
        this.whichDay = this.whichDay.bind(this);
    }

    componentDidMount() {
        this.props.obtUserData();
    }

    whichDay(curDate, lastDate, dataArr) {
        var deltaDateSec = Date.parse(curDate) - Date.parse(lastDate);
        // console.log(deltaDateSec);
        var SingleDaySec = 86400 * 1000;
        for (var i = dataArr.length - 1; i >= 0; i--) {
            if (!dataArr[i]) {
                dataArr[i] = 0;
            }
            if (deltaDateSec >= i * SingleDaySec && deltaDateSec < (i + 1) * SingleDaySec) {
                dataArr[i]++;
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.studied != this.props.studied) {
            // console.log(this.props.studied);
            // console.log(nextProps.studied);
            var studiedTemp = nextProps.studied;

            var dataArr = [];
            dataArr.length = 7;
            var dateArr = [];
            dateArr.length = 7;
            var curDate = new Date();
            curDate.setHours(23, 59, 59, 999);
            // console.log("curDate");
            for (var i = 0; i < dateArr.length; i++) {
                var lastDate = new Date(Date.parse(curDate) - 86400 * 1000 * i);
                var month = lastDate.getMonth() + 1;
                var date = lastDate.getDate();
                dateArr[i] = `${month}/${date}`
            }
            studiedTemp.map((curValue, index)=> {
                if (curValue.passTime) {
                    var d = new Date(curValue.passTime);
                    this.whichDay(curDate, d, dataArr)
                }
            });

            var dataArrRev = dataArr.reverse();
            var dateArrRev = dateArr.reverse();

            var data = {
                labels: dateArrRev,
                datasets: [
                    {
                        label: "学习量",
                        backgroundColor: 'rgba(54, 162, 235, 0.2)',
                        borderColor: 'rgba(54, 162, 235, 1)',
                        borderWidth: 1,
                        data: dataArrRev,
                    }
                ]
            };

            var options = {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            };

            var ctx = this.refs.myChart;
            var myLineChart = new Chart(ctx, {
                type: 'line',
                data: data,
                options: options
            });

        }
    }

    render() {
        const {headImgUrl, curRank, nikeName, studied, curStudiedCount, studiedLoadCount, loadMoreStudiedCards,studiedFetching}= this.props;

        return (
            <div className="userInfo">
                <div className="userInfo-head">
                    <img
                        src={headImgUrl}
                        className="headImg"/>
                    <h4 className="userInfo-userName">{nikeName}</h4>
                    <div className="userInfo-studiedData">
                        <div className="userInfo-studiedData-total">
                    <span className="curRank">
                        {curRank}
                    </span>
                            <span className="studiedData-label">
                        当前排名
                    </span>
                        </div>
                        <div className="userInfo-studiedData-total">
                    <span className="curCount">
                        {curStudiedCount}
                    </span>
                            <span className="studiedData-label">
                        学习总量
                    </span>
                        </div>
                    </div>
                </div>

                <canvas ref="myChart" className="userInfo-chart" height="210"></canvas>

                <StudiedRecord studied={studied} studiedLoadCount={studiedLoadCount}
                               loadMoreStudiedCards={loadMoreStudiedCards}
                               studiedFetching={studiedFetching} curStudiedCount={curStudiedCount}/>
            </div>
        )
    }
}

export
default
UserInfo;