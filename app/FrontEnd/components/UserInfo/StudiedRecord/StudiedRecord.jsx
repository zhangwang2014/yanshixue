import React from 'react';
require('./StudiedRecord.scss');

class StudiedRecord extends React.Component {
    constructor(props) {
        super(props);
        // this.studiedList =null;
        // this.firstTimeReactBottom = true;
        // this.timer = false;
        // this.reachBottom = this.reachBottom.bind(this);
        // this.listScroll = this.listScroll.bind(this);
    }

    render() {
        const {studied, studiedLoadCount, studiedFetching, loadMoreStudiedCards, curStudiedCount} = this.props;
        var studiedTemp = [];
        studied.map((curValue,index)=>{
            studiedTemp[index] = curValue
        });
        studiedTemp.reverse();
        return (
            <div className="studiedRecord">
                <h4 className="studiedRecord-title">学习记录</h4>
                <ul className="studiedList">
                    {
                        studiedTemp.map((curValue, index)=> {
                            if (index >= 0 && curValue.passTime && index <= studiedLoadCount) {
                                var d = new Date(curValue.passTime);
                                return (<li className="studiedRecord-item" key={`learningRecord${index}`}>
                                    <span className="videoName"> {curValue.videoName}</span>
                                    <span className="date">{`${d.getMonth() + 1}月${d.getDate()}日`}</span>
                                </li>)
                            }
                        })
                    }

                    <div className={studiedFetching||(studiedLoadCount>=curStudiedCount)? "hidden" : "loadMore"}
                         onClick={loadMoreStudiedCards}>加载更多
                    </div>
                    <div className={studiedFetching ? "loading" : "hidden"}>正在加载中...</div>
                </ul>
            </div>
        )
    }
}

export default StudiedRecord;