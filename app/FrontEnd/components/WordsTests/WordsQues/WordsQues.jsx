import React from 'react';
require('./WordsQues.scss');
import TransQue from './TransQue/TransQue';
import ChoiceQue from './ChoiceQue/ChoiceQue';

class WordsQues extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {sectionOver, quesData, optClickState,testOver, optClick, cardIndex,showTransResult} = this.props;
        return (
            <div className="wordsQues">
                {quesData.queType == 'trans' ?
                    <TransQue transContent={quesData.transContent}
                              showResult={quesData.showResult}
                              transResult={quesData.transResult}
                              showTransResult={showTransResult}/> :
                    <ChoiceQue cardIndex={cardIndex}
                               testStem={quesData.testStem}
                               testTitle={quesData.testTitle}
                               options={quesData.options}
                               optClickState={optClickState}
                               optClick={optClick}/>}
                <div className={testOver ? "testOver" : "hidden"}>
                    <span className="weui-btn weui-btn_mini weui-btn_primary" onClick={sectionOver}>测试结束</span>
                </div>

            </div>
        )
    }
}

export default WordsQues;