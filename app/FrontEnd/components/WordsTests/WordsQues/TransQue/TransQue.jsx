import React from 'react';
require ('./TransQue.scss');
// const TransQue = (transContent, showResult, transResult, showTransResult)=>(
//     <div className="transQue">
//         <div className="transContent" dangerouslySetInnerHTML={{__html: transContent}}>
//         </div>
//         <div className={showResult ? "transResult" : "hidden"}
//              dangerouslySetInnerHTML={{__html: transResult}}>
//         </div>
//         <div className={showResult ? "hidden" : "showResultBtn"}>
//             <span className="weui-btn weui-btn_mini weui-btn_default" onClick={showTransResult}>显示翻译</span>
//         </div>
//     </div>
// );

class TransQue extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {transContent, showResult, transResult, showTransResult} = this.props;

        return (
            <div className="transQue">
                <div className="transContent" dangerouslySetInnerHTML={{__html: transContent}}>
                </div>
                <div className={showResult ? "transResult" : "hidden"}
                     dangerouslySetInnerHTML={{__html: transResult}}>
                </div>
                <div className={showResult ? "hidden" : "showResultBtn"}>
                    <span className="weui-btn weui-btn_mini weui-btn_default" onClick={showTransResult}>显示翻译</span>
                </div>
            </div>
        )
    }
}

export default TransQue;