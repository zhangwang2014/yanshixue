import React from 'react';
import Done from 'react-icons/lib/md/done';

require('./ChoiceQue.scss');

class ChoiceQue extends React.Component {
    constructor(props) {
        super(props);
    }



    render() {
        const {testStem, testTitle, options, optClickState, optClick, cardIndex} = this.props;

        console.log(cardIndex);
        return (
            <div className="choiceQue">
                <div className="choiceQue-stem" dangerouslySetInnerHTML={{__html: testStem}}>
                </div>

                <div className="choiceQue-title">
                    {testTitle}
                </div>

                {options.map((opt, index)=> (
                        <div className={optClickState[index] && !opt.testOptState ? "choiceQue-opt answerWrong" : "choiceQue-opt"}
                             key={`opt${index}`}
                             onClick={optClickState[index] ? "" : optClick.bind(this,cardIndex,index)}>
                            <span className={opt.testOptState && optClickState[index] ? "optContent optRight" : "optContent"}>{opt.testOptContent}</span>
                            {opt.testOptState && optClickState[index] ?
                                <span className="answerRight"><Done/></span> : ""}
                        </div>
                    )
                )}

            </div>
        )
    }
}

export default ChoiceQue;