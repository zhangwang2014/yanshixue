import React from 'react';
require('./WordsReview.scss');

class WordsReview extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {wordsReviewState, reviewWordsData, doQues, reviewWord} = this.props;
        return (
            <div className="wordsReview">
                {wordsReviewState ? <WordsReviewStage2 reviewWordsData={reviewWordsData} doQues={doQues}/> :
                    <WordsReviewStage1 reviewWordsData={reviewWordsData} reviewWord={reviewWord}/>}
            </div>
        )
    }
}

class WordsReviewStage1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            time: 10,
        });
        this.timeEnd = this.timeEnd.bind(this);
        this.handleTouchStart = this.handleTouchStart.bind(this);
        this.handleTouchEnd = this.handleTouchEnd.bind(this);
    }

    componentDidMount() {
        this.timeFlag = setInterval(function () {
            if (this.state.time === 0) {
                this.timeEnd();
                return;
            }
            this.setState({time: this.state.time - 1});
        }.bind(this), 1000);
    }

    timeEnd() {
        clearInterval(this.timeFlag);
        this.props.reviewWord();
    }

    handleTouchStart() {
        clearInterval(this.timeFlag);
    }

    handleTouchEnd() {
        this.timeFlag = setInterval(function () {
            if (this.state.time === 0) {
                this.timeEnd();
                return;
            }
            this.setState({time: this.state.time - 1});
        }.bind(this), 1000);
    }

    render() {
        const {reviewWordsData} = this.props;

        const reviewWords = reviewWordsData.map(
            (reviewWord, index)=>(
                <div className="reviewWord" key={reviewWord[0] + index}>
                    <div className="reviewWordContent">{ reviewWord[0] + ":" + reviewWord[1] }</div>
                </div>
            )
        );

        return (
            <div className="wordsReviewStage1">
                    <h4 className="wordsReview-title">单词速记</h4>
                    <div className="reviewWords" onTouchStart={this.handleTouchStart}
                         onTouchEnd={this.handleTouchEnd}>
                        {reviewWords}
                    </div>
                    <div className="wait-time">
                        {"还剩" + this.state.time + "秒强化记忆" }
                    </div>
            </div>
        )
    }
}

class WordsReviewStage2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            alreadyTestedNum: 0
        });
        this.alreadyTested = this.alreadyTested.bind(this);
    }

    alreadyTested() {
        this.state.alreadyTestedNum++;
        this.setState({});
    }

    render() {
        return (
            <div className="WordsReviewStage2">
                <h4 className="wordsReview-title" onTouchEnd={this.props.touchEnd}>词汇检测</h4>
                <div className="reviewWordItem">

                    {this.props.reviewWordsData.map((item, idx) => (
                        <WordItem word={item} alreadyTested={this.alreadyTested} key={item}/>
                    ))}

                    <div className={(this.state.alreadyTestedNum == this.props.reviewWordsData.length) ? "ok-btn" : "hidden"}
                        >
                        <span className="weui-btn weui-btn_mini weui-btn_default"  onClick={this.props.doQues}>做题强化</span>
                    </div>
                </div>
            </div>
        );
    }
}

class WordItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            isShow: false,
            maskHeight: 0,
            clickNum: 0,
        });
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        let answer = this.refs.answer;
        this.setState({
            maskHeight: answer.offsetHeight//初始化遮罩层高度等于对应答案的高度
        });
    }

    handleClick() {
        if (this.state.clickNum == 5) {
            return false;//之后点击一直等于5
        }
        if (this.state.clickNum == 4) {
            this.props.alreadyTested();
            this.setState({
                isShow: true,
            });
        }
        this.setState({
            clickNum: this.state.clickNum + 1,
            isShake: true,
        });
        setTimeout(function () {
            this.setState({
                isShake: false,
            });
        }.bind(this), 500);
    }

    render() {
        return (
            <div className="word-item">
                <div className={"til" + (this.state.isShow ? " star" : "")}>{ this.props.word[0] }的意思是</div>
                <div className={"answer" + (this.state.isShake ? " shake" : "")} ref="answer"
                     onTouchStart={this.handleClick}>
                    {this.props.word[1]}
                    {!this.state.isShow ?
                        <div
                            className="answer-mask"
                            style={{
                                height: this.state.maskHeight + "px",
                                lineHeight: this.state.maskHeight + "px",
                                opacity: ((5 - this.state.clickNum) / 5)
                            }}>
                            使劲点击查看答案
                        </div>
                        : null
                    }
                </div>
            </div>
        );
    }
}

export default WordsReview;



