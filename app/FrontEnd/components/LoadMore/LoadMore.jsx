import React from 'react';

const LoadMore = ()=>(
    <div className="weui-loadmore">
        <i className="weui-loading"></i>
        <span className="weui-loadmore__tips">正在加载</span>
    </div>
);

const NoMore = ()=>(
    <div class="weui-loadmore weui-loadmore_line">
        <span class="weui-loadmore__tips">暂无数据</span>
    </div>
);

export default LoadMore;