import React from 'react';
require ('./Search.scss');

class Search extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {searchBarFocused, searchBarFocus, searchBarBlur} = this.props;

        var style = {
            searchLable: {
                transformOrigin: "0px 0px 0px",
                opacity: 1,
                transform: "scale(1, 1)"
            }
        };

        return (
            <div className="searchPage">
                <div className={searchBarFocused ? "weui-search-bar weui-search-bar_focusing" : "weui-search-bar"}
                     onClick={searchBarFocus}>
                    <form className="weui-search-bar__form">
                        <div className="weui-search-bar__box">
                            <i className="weui-icon-search searchIcon"></i>
                            <input type="search" className="weui-search-bar__input" placeholder="搜索" required=""
                                   onBlur={searchBarBlur}/>
                            <a href="javascript:" className="weui-icon-clear"></a>
                        </div>
                        <label className="weui-search-bar__label" style={style.searchLable}>
                            <i className="weui-icon-search"></i>
                            <span>搜索功能尚待完善</span>
                        </label>
                    </form>
                    <a href="javascript:" className="weui-search-bar__cancel-btn cancel-btn" onClick={searchBarBlur}>取消</a>
                </div>
                <div className="weui-toast">
                    <i className="weui-loading weui-icon_toast"></i>
                    <p className="weui-toast__content">尚在开发<br/>敬请期待</p>
                </div>
            </div>
        )
    }
}

// const Search = ()=>(
//
// );

export default Search;