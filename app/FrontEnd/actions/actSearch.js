import {request} from '../public/js/lib';

export const searchBarFocus =()=>{
    return{
        type:'SEARCH_BAR_FOCUS',
    }
};

export const searchBarBlur = ()=>{
    return{
        type:"SEARCH_BAR_BLUR"
    }
};
