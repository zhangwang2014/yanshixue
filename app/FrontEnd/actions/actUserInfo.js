import {request} from '../public/js/lib';

export const obtUserData = ()=> {
    return (dispatch)=> {
        dispatch({type: 'OBT_USER_Data'});
        return request('/obtUserData').then(res=>res.json()).then(res=> {
            console.log(res.resData);
            dispatch({type: 'OBT_USER_DATA_SUCCESS', payload: res.resData})
        })
    }
};

export const loadMoreStudiedCards = ()=>{
    return (dispatch)=>{
        dispatch({type:"LOAD_MORE_STUDIED_CARDS"});
        setTimeout(function () {
            return(
                dispatch({type:"LOAD_MORE_FINISHED"})
            )
        },1000)
    }
};