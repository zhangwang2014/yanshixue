import {request} from '../public/js/lib';

export const cardToggle = (index)=> {
    return {
        type: 'CARD_TOGGLE',
        index
    }
};

export const showLoading = ()=>{
   return{
       type:"SHOW_LOADING"
   }
};

export const showMoreCards = ()=> {
    return{
        type:"SHOW_MORE_CARDS"
    }
};

export const skipVideo = (index)=> {
    return {
        type: "SKIP_VIDEO",
        index
    }
};

export const doQues = (index)=> {
    return {
        type: "DO_QUES",
        index
    }
};


export const obtCardsData = ()=> {
    return (dispatch) => {
        dispatch({type: 'OBT_CARD_DATA'});
        return request('/data').then(res=>res.json()).then(res=> {
            // console.log(res.cardsData);
            dispatch({type: 'OBT_CARD_DATA_SUCCESS', payload: res.cardsData})
        })
    }
};

export const reviewWord = (index)=> {
    return {
        type: "REVIEW_WORD",
        index
    }
};

export const optClick = (index, optIndex)=> {
    return {
        type: "OPT_CLICK",
        index,
        optIndex
    }
};

// export const ObtCardsDataSuccess = ()=>{
//     return{
//         type:"OBT_CARD_DATA_SUCCESS",
//         payload:cardData
//     }
// };
//
// export const ObtCardsDataFailure=(error)=>{
//     return{
//         type:"OBT_CARD_DATA_FAILURE",
//         payload:error
//     }
// };

export const videoWatched = (index)=> {
    return {
        type: "VIDEO_WATCHED",
        index
    }
};

export const sectionOver = (index)=> {
    return {
        type: "SECTION_OVER",
        index
    }
};

export const showTransResult = (index)=> {
    return {
        type: "SHOW_TRANS_RESULT",
        index
    }
};


