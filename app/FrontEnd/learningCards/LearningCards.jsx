import React from 'react';
import VideoPlayer, {Component} from '../videoPlayer/VideoPlayer';
import WordsTests from '../WordsTests/WordsTests';
import WordQues from '../WordsTests/WordsQues/WordQues';

import ReadingTests from '../ReadingTests/ReadingTests';

import Snackbar from 'material-ui/Snackbar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';


import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import CloseIcon from 'material-ui/svg-icons/navigation/close';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import LinearProgress from 'material-ui/LinearProgress';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import SkipNext from 'material-ui/svg-icons/av/skip-next';
import {deepOrange300} from 'material-ui/styles/colors';

import {request} from '../../lib';

import CircularProgress from 'material-ui/CircularProgress';
import Badge from 'material-ui/Badge';
import Paper from 'material-ui/Paper';

// var injectTapEventPlugin = require("react-tap-event-plugin");
// injectTapEventPlugin();


import MobileDetect from 'mobile-detect';

require('./LearningCards.scss');

var MaterialCards = React.createClass({

    getInitialState(){

        return {
            theme: lightBaseTheme
        }
    },

    changeTheme(){
        this.state.theme = ( (this.state.theme == lightBaseTheme) ? darkBaseTheme : lightBaseTheme);
        this.setState({});
    },

    render(){
        return (
            <div className="material-cards">
                <MuiThemeProvider muiTheme={getMuiTheme(this.state.theme)}>
                    <LearningCards changeTheme={this.changeTheme}/>
                </MuiThemeProvider>
            </div>
        )
    }
});

var LearningCards = React.createClass({

    getInitialState: function () {
        return {
            data: [],
            studiedCount: 0,
            showVideoCard: [],
            animatedName: "cards",
            refreshTooFast: false,
            refreshTooFastClassName: "refreshTooFast",
            timeProgress: 0,
            curCardVideoLength: 0,
            showIncreasedTime: false,
            showDeleteAlert: false,
            loadCardsNumber: 8,
            firstTimeReactBottom: true,
            timer: false,
            headerScrollTop: 0,
            menuState: false,
            showHelpFile: false,
            rankData: {
                showRankBoard: false,
                rankBoardData: {}
            },
            user_id: "",
            user_name: ""
        }
    },

    componentWillMount: function () {

        request('/getUserInfo').then(res=>res.json()).then(res=> {
            this.setState({
                user_id: res[0],
                user_name: res[1]
            })
        }).then(res=> {
            // console.log("123123");
            daovoice('init', {
                app_id: "7b061713",
                user_id: this.state.user_id, // 必填: 该用户在您系统上的唯一ID
                // email: "zhangwang2014@icloud.com", // 选填:  该用户在您系统上的主邮箱
                name: this.state.user_name, // 选填: 用户名
                // signed_up: 1449821660 // 选填: 用户的注册时间，用Unix时间戳表示
            });
            //
            // daovoice('update');
        })
    },

    componentDidMount: function () {

        request('/data').then(res=>res.json()).then(res=> {
            // console.log(res);
            this.setState({
                data: res.tempData,
                studiedCount: res.studiedCount
            });
            console.log(res);
        });

        this.refs.cardsList.addEventListener('scroll', this.pageScroll);
    },

    pageScroll: function () {

        if (this.reachBottom()()) {
            // console.log("refreshTooFast");

            if (this.state.loadCardsNumber < 13) {
                this.state.refreshTooFast = false;
                this.state.refreshTooFastClassName = "refreshTooFast";
                this.setState({});
                setTimeout(this.loadMoreCards, 1000);
            } else {
                this.state.refreshTooFast = true;
                this.state.refreshTooFastClassName = "refreshTooFast";
                this.setState({});
                setTimeout(()=> {
                    this.state.refreshTooFastClassName = "hidden";
                    this.setState({});
                }, 5000);
                // // console.log("喂,那么多卡片没看呢!")
            }
        }
    },

    findIndexByTitle: function (cardTitle) {
        var i;
        for (var a = 0; a < this.state.data.length; a++) {
            if (this.state.data[a].title == cardTitle) {
                i = a;
                return (i);
            }
        }
    },

    cardExpand: function (cardTitle) {
        var i = this.findIndexByTitle(cardTitle);
        this.state.showVideoCard[i] = true;
        this.setState({});
    },

    cardShrink: function (cardTitle) {
        var i = this.findIndexByTitle(cardTitle);
        this.state.showVideoCard[i] = false;
        this.setState({});
    },

    reachBottom: function () {

        return ()=> {
            var reachBottom = (this.refs.cardsList.scrollHeight - this.refs.cardsList.scrollTop) <= (this.refs.cardsList.clientHeight) + 1;
            if (reachBottom) {
                if (this.state.firstTimeReactBottom) {
                    this.state.firstTimeReactBottom = false;
                    this.setState({});
                    return true;
                }
                if (this.state.timer) {
                    return false;
                }
                this.state.timer = true;
                this.setState({});
                setTimeout(()=> {
                    this.state.timer = false;
                    this.state.firstTimeReactBottom = true;
                    this.setState({});
                }, 1000);
            }
            return reachBottom;
        };
        // return ((this.refs.cardsList.scrollHeight - this.refs.cardsList.scrollTop) <= (this.refs.cardsList.clientHeight)+1);
    },

    loadMoreCards: function () {
        var addCardNumber = 2;
        if (this.state.loadCardsNumber + addCardNumber > 13) {
            addCardNumber = 13 - this.state.loadCardsNumber;
        }
        this.state.loadCardsNumber = this.state.loadCardsNumber + addCardNumber;
        this.setState({});
    },

    addStudiedCount(){
        this.state.studiedCount++;
        console.log("studiedCount");
        this.setState({});
    },

    sectionOver: function (cardTitle) {
        var translateDistanceY = this.refs[cardTitle].curCardHeight + 9;
        var i = this.findIndexByTitle(cardTitle);

        this.setState({
            curCardVideoLength: this.state.data[i].videoLength,
            showIncreasedTime: true
        });

        //传送删除视频时长到后端
        // var curCardVideoLength = this.state.data[i].videoLength;
        var cardType = this.state.data[i].type;

        var scrollTop = this.refs.learningCards.scrollTop;
        var scrollHeight = this.refs.learningCards.scrollHeight;
        var offsetHeight = this.refs.learningCards.offsetHeight;


        this.state.data.splice(i, 1);
        this.state.showVideoCard.splice(i, 1);

        requestAnimationFrame(()=> {
            requestAnimationFrame(()=> {
                this.state.animatedName = "cards animated";
                this.state.data.forEach((d, a)=> {
                    if (a >= i) {
                        this.state.data[a].transformDistance = 0 + "px";
                    }
                });
                this.setState({});
            });
        });

        if ((scrollTop + offsetHeight) > scrollHeight - translateDistanceY) {

            var t0;
            requestAnimationFrame((function nextFrame(t) {
                if (!t0) {
                    t0 = t;
                }
                if (t - t0 <= 500) {
                    var newScroll = (t - t0) / 500 * translateDistanceY;
                    this.refs.learningCards.scrollTop = scrollTop - newScroll;
                    // // console.log(t-t0, newScroll);
                    requestAnimationFrame(nextFrame.bind(this));
                }
            }).bind(this));

        }


        this.state.data.forEach((d, a)=> {
            if (a >= i) {
                this.state.data[a].transformDistance = translateDistanceY + "px";
            }
        });
        this.setState({});

        setTimeout(()=> {
            this.state.animatedName = "cards";
            this.setState({});
        }, 500);

        // var curDate = new Date();
        // var curTime = curDate.getTime();

        var body = {};
        body.cardTitle = cardTitle;
        body.cardType = cardType;
        // body.curTime

        // request('/sectionOver', body).then(res=>res.json()).then(res=> {
        //     // console.log("res.allType" + res.allType);

        // });

        setTimeout(()=> {
            this.setState({
                // timeProgress: res.allType,
                showIncreasedTime: false
            });
        }, 2000);

        this.state.loadCardsNumber = this.state.loadCardsNumber - 1;
        if (this.state.loadCardsNumber < 6) {
            this.state.loadCardsNumber = this.state.loadCardsNumber + 2;
        }

    },

    deleteCard: function (cardTitle) {
        // // console.log("执行到deleteCard");

        var translateDistanceY = this.refs[cardTitle].curCardHeight + 9;
        var i = this.findIndexByTitle(cardTitle);
        this.setState({
            showDeleteAlert: true
        });

        //传送删除视频时长到后端
        var curCardVideoLength = this.state.data[i].videoLength;
        var curType = this.state.data[i].type;

        var scrollTop = this.refs.learningCards.scrollTop;
        var scrollHeight = this.refs.learningCards.scrollHeight;
        var offsetHeight = this.refs.learningCards.offsetHeight;


        this.state.data.splice(i, 1);
        this.state.showVideoCard.splice(i, 1);

        requestAnimationFrame(()=> {
            requestAnimationFrame(()=> {
                this.state.animatedName = "cards animated";
                this.state.data.forEach((d, a)=> {
                    if (a >= i) {
                        this.state.data[a].transformDistance = 0 + "px";
                    }
                });
                this.setState({});
            });
        });

        if ((scrollTop + offsetHeight) > scrollHeight - translateDistanceY) {

            var t0;
            requestAnimationFrame((function nextFrame(t) {
                if (!t0) {
                    t0 = t;
                }
                if (t - t0 <= 500) {
                    var newScroll = (t - t0) / 500 * translateDistanceY;
                    this.refs.learningCards.scrollTop = scrollTop - newScroll;
                    // // console.log(t-t0, newScroll);
                    requestAnimationFrame(nextFrame.bind(this));
                }
            }).bind(this));

        }


        this.state.data.forEach((d, a)=> {
            if (a >= i) {
                this.state.data[a].transformDistance = translateDistanceY + "px";
            }
        });
        this.setState({});

        setTimeout(()=> {
            this.state.animatedName = "cards";
            this.setState({});
        }, 500);
        var curDate = new Date();
        var curTime = curDate.getTime();

        request('/delete', [cardTitle, curTime, curCardVideoLength, curType]).then(res=>res.json()).then(res=> {
            // console.log("res.allType" + res.allType);
            setTimeout(()=> {
                this.setState({
                    showDeleteAlert: false
                });
            }, 2000);
        });

        this.state.loadCardsNumber = this.state.loadCardsNumber - 1;
        if (this.state.loadCardsNumber < 6) {
            this.state.loadCardsNumber = this.state.loadCardsNumber + 2;
        }

    },

    recordingScrollTop: function () {
        this.state.headerScrollTop = this.refs.cardsList.scrollTop;
        this.setState({});
        // console.log(this.refs.cardsList.scrollTop);
    },

    changeCardPosition: function () {
        this.refs.cardsList.scrollTop = this.state.headerScrollTop;
    },

    menuOpen: function () {

        this.setState({
            menuState: true
        });

        console.log("menuOpen")
    },

    menuClose: function () {
        this.setState({
            menuState: false
        });
        console.log("menuClose")
    },

    changeTheme: function () {
        this.menuClose();
        this.props.changeTheme();
    },

    openHelp: function () {
        this.setState({
            showHelpFile: true,
            menuState: false
        })
    },

    openRankBoard: function () {
        this.state.rankData.showRankBoard = true;
        this.state.menuState = false;
        this.setState({});
        this.ranking();
    },

    closeHelp: function () {
        this.setState({
            showHelpFile: false
        })
    },

    leaveMessage: function () {
        // daovoice('init', {
        //     app_id: "7b061713",
        //     user_id: this.state.user_id, // 必填: 该用户在您系统上的唯一ID
        //     // email: "zhangwang2014@icloud.com", // 选填:  该用户在您系统上的主邮箱
        //     name: this.state.user_name, // 选填: 用户名
        //     // signed_up: 1449821660 // 选填: 用户的注册时间，用Unix时间戳表示
        // });
        // //
        daovoice('update');

        daovoice('openNewMessage');
        // this.setState({
        //     menuState: false
        // });

    },

    ranking: function () {
        request('/userAction/ranking').then(res=>res.json()).then(res=> {
            console.log(res.resData);
            this.state.rankData.rankBoardData = res.resData;
            this.setState({});
            console.log("执行了");
        })
        console.log("没执行");

    },

    closeRankBoard: function () {
        this.state.rankData.showRankBoard = false;
        this.setState({});
    },

    render(){
        var paintCard = ()=> {
            return (
                <TopicCard cardType={type} title={cardTitle} describe={cardDes} videoLength={videoLength}
                           shortCut={shortCut} key={key + "video"} display={display}
                           cardExpand={this.cardExpand.bind(this, cardTitle)}
                           cardShrink={this.cardShrink.bind(this, cardTitle)}
                           videoSrc={videoSrc}
                           deleteCard={this.deleteCard.bind(this, cardTitle)}
                           sectionOver={this.sectionOver.bind(this, cardTitle)}
                           animatedName={this.state.animatedName}
                           transformDistance={transformDistance}
                           ref={cardTitle} words={words}
                           labels={labels} readingTests={readingTests}
                           recordingScrollTop={this.recordingScrollTop}
                           changeCardPosition={this.changeCardPosition}
                           menuClose={this.menuClose}
                           wordQues={wordQues}
                           addStudiedCount={this.addStudiedCount}/>)
        };

        var loadMoreMark = ()=> {
            return (
                <LoadMoreMark refreshTooFast={this.state.refreshTooFast }
                              refreshTooFastClassName={this.state.refreshTooFastClassName}
                              key={this.state.refreshTooFast}/>
            )
        };

        var showCards = [];
        var allCards = this.state.data.slice(0, this.state.loadCardsNumber);

        for (var i = 0; i < allCards.length; i++) {
            var type = allCards[i].type;
            var cardTitle = allCards[i].title;
            var cardDes = allCards[i].describe;
            var videoLength = allCards[i].videoLength;
            var shortCut = allCards[i].shortCut;
            var videoSrc = allCards[i].videoSrc;
            var display = this.state.showVideoCard[i];
            var transformDistance = allCards[i].transformDistance;
            var wordQues = allCards[i].wordQues;

            if (allCards[i].words) {
                var words = allCards[i].words;
            } else {
                var words = [["marvel", "v. 对…惊奇"], ["precise", "adj. 精确的；明确的；严格的"], ["ornamentation", "n. 装饰,装饰品"], ["replacement", "n. 代替, 替换, 更换"], ["marvelously", "adv. 令人惊讶地"]]
            }
            if (allCards[i].labels) {
                var labels = allCards[i].labels;
            } else {
                var labels = [["label1"], ["label2"], ["label3"]]
            }
            if (allCards[i].readingTests) {
                var readingTests = allCards[i].readingTests
            } else {
                var readingTests = [
                    ["Architecture is a three-dimensional form. It utilizes space, mass, texture, line, light, and color. To be architecture, a building must achieve a working harmony with a variety of elements. Humans instinctively seek structures that will shelter and enhance their way of life. It is the work of architects to create buildings that are not simply constructions but also offer inspiration and delight. Buildings contribute to human life when they provide shelter, enrich space, complement their site, suit the climate, and are economically<span class='emphasesText'> feasible</span> . The client who pays for the building and defines its function is an important member of the architectural team. The mediocre design of many contemporary buildings can be traced to both clients and architects.", "The word “feasible” in the passage is closet in meaning to ", "In existence", "Without question", "Achievable", "Most likely", "原文中 “feasible” 所在句子为：“Buildings contribute to human life when they provide shelter, enrich space, complement their site, suit the climate, and are economically feasible.”可以理解为：“建筑物为人类的生活提供了遮蔽和丰富的空间，增加人们的活动场所、帮助人们适应气候的变化，同时经济上（建造的花费）也可承受。” “when”引导的从句中“provide, enrich, complement”和“are”并列，都是“they”的谓语，因此 “feasible”应是表示正面、好处的词汇", "选项为中性词", "选项程度过高", "选项贴合原意", "选项与“economically”搭配在此不恰当", "Achievable"],
                    ["Tunas, mackerels, and billfishes (marlins, sailfishes, and swordfish) swim continuously. Feeding, courtship, reproduction, and even 'rest' are carried out while in constant motion. As a result, practically every aspect of the body form and function of these swimming 'machines' is adapted to <span class='emphasesText'> enhance</span>their ability to swim. ", "The word enhance in the passage is closest in meaning to", "Use", "Improve", "Counteract", "Balance", "原文该单词所在句子为：“Humans instinctively seek structures that will shelter and enhance their way of life. ”可以理解为：“人类本能地寻求可以提供居住并且他们______生活质量的建筑。”整个段落都是在讲建筑的功用和贡献，可推出“enhance”必定是个褒义词。我们可以从下文找到同义句：“enrich space, complement their site (增加人们的活动场所，完善人们的居所）", "use使用,与语义不符", "Improve提高改善,符合语义", "Counteract抵消抵制,与语义不符", "Balance平衡,与语义不符", "Improve"],
                    ["<span class='emphasesText'>In order for the structure to achieve the size and strength necessary to meet its purpose, architecture employs methods of support that, because they are based on physical laws, have changed little since people first discovered them-even while building materials have changed dramatically.</span> The world’s architectural structures have also been devised in relation to the objective limitations of materials. Structures can be analyzed in terms of how they deal with downward forces created by gravity. They are designed to withstand the forces of compression (pushing together), tension (pulling apart), bending, or a combination of these in different parts of the structure.", "Which of the sentences below best expresses the essential information in the highlighted sentence in the passage? Incorrect choices change the meaning in important ways or leave out essential information.", "Unchanging physical laws have limited the size and strength of buildings that can be made with materials discovered long ago.", "Building materials have changed in order to increase architectural size and strength, but physical laws of structure have not changed.", "When people first started to build, the structural methods used to provide strength and size were inadequate because they were not based on physical laws.", "Unlike building materials, the methods of support used in architecture have not changed over time because they are based on physical laws.", "In order for the structure to achieve the size and strength necessary to meet its purpose, architecture employs methods of support that, because they are based on physical laws, have changed little since people first discovered hem—even while building materials have changed dramatically.画线句子翻译为“为了使建筑的结构满足其建造目的所要求的规模和强度，建筑会采用一些支撑方法，这些方法因为基于物理定律而从始至终没有发生什么变化，虽然建筑材料已经发生了翻天覆地的变化。”句子的主要逻辑是因果关系和对比转折", "“physical laws” 作用的对象是 “method of support”，而不是“the size and strength of buildings”，所以 A 选项错误", "原句对比的是 “method of support”和“building material”,而非“physical laws”和“building material”，所以B选项错误", "C选项明显改变原句意思，错误", "D选项正确表达了原句中的主要信息和主要逻辑：“building material”变化大，而相比之下“method of support”变化很小，原因是“method of support”要基于 “physical laws”。因此正确答案为D选项", "Unlike building materials, the methods of support used in architecture have not changed over time because they are based on physical laws."]
                ]
            }
            var key = allCards[i].title;
            let oneCard = paintCard();
            showCards.push(oneCard);
        }

        var loadMoreCard = loadMoreMark();
        showCards.push(loadMoreCard);

        var styles = {
            CircularProgress: {
                display: "inline-block",
                position: "absolute",
                right: "33%",
                top: "-0.5rem"
            },

            header: {
                width: "100%",
                flex: 1
            }
        };

        return (
            <div className="learningCards" ref="learningCards">
                <Header ref="header" timeProgress={this.state.timeProgress}
                        curCardVideoLength={this.state.curCardVideoLength}
                        showIncreasedTime={this.state.showIncreasedTime}
                        showDeleteAlert={this.state.showDeleteAlert}
                        changeTheme={this.changeTheme}
                        style={styles.header} menuState={this.state.menuState}
                        menuOpen={this.menuOpen} menuClose={this.menuClose} openHelp={this.openHelp}
                        leaveMessage={this.leaveMessage} studiedCount={this.state.studiedCount}
                        openRankBoard={this.openRankBoard} rankBoardData={this.state.rankData.rankBoardData}/>
                <div className="cardsList scrollable" ref="cardsList">
                    {showCards}
                </div>
                <HelpFile showHelpFile={this.state.showHelpFile} closeHelp={this.closeHelp} menuClose={this.menuClose}/>
                <RankingBoard showRankBoard={this.state.rankData.showRankBoard} closeRankBoard={this.closeRankBoard}
                              menuClose={this.menuClose} rankBoardData={this.state.rankData.rankBoardData}/>
            </div>
        )
    }
});

var LoadMoreMark = React.createClass({
    getInitialState(){
        return ({
            iOS: false
        })
    },

    componentDidMount(){
        var md = new MobileDetect(window.navigator.userAgent);
        // console.log( "isIphone"+"  "+ md.is('iPhone'));
        // console.log(md.os());
        if (md.os() == "iOS") {
            this.setState({
                iOS: true
            })
        }
    },

    render(){
        var styles = {
            CircularProgress: {
                display: "inline-block",
                position: "absolute",
                right: "33%",
                top: "-0.5rem"
            }
        };

        // console.log(this.state.iOS);
        // console.log(this.props.refreshTooFast);

        return (
            <div className={this.state.iOS ? "loadMoreMark loadMoreMarkIOS" : "loadMoreMark"}>
                {this.props.refreshTooFast ? (
                    <div className={this.props.refreshTooFastClassName}><p>喂,那么多卡片没看呢!</p></div>) : (
                    <div className="loadMore">
                        <p>正在加载</p>
                        <CircularProgress size={0.4} style={styles.CircularProgress}/>
                        {/*<img src="../../dist/public/img/loadding.gif" alt=""/>*/}
                    </div>)}
            </div>
        )
    }
});


var TopicCard = React.createClass({

    startY: 0,
    startX: 0,
    deviceWidth: 0,
    moveOnNextFrame: false,
    verticalMove: false,
    horizontalMove: false,
    curCardHeight: 0,

    getInitialState: () => {
        return ({
            videoEnd: false,
            videoSkip: false,
            wordReview: false
        })
    },

    touchMove(event) {
        if (event.targetTouches.length == 1) {
            var touch = event.targetTouches[0];
            var deltaX = touch.pageX - this.startX;


            var absDeltaX = Math.abs(deltaX);
            var absDeltaY = Math.abs(touch.pageY - this.startY);
            // // console.log(deltaX, absDeltaY, this.verticalMove, this.horizontalMove);

            if (!this.verticalMove && !this.horizontalMove) {
                if (absDeltaX < 20 && absDeltaY >= 20) {
                    this.verticalMove = true;
                } else if (absDeltaX >= 20 && absDeltaY < 20) {
                    this.horizontalMove = true;
                } else if (absDeltaX >= 20 && absDeltaY >= 20) {
                    if (absDeltaX > absDeltaY) {
                        this.horizontalMove = true;
                    } else {
                        this.verticalMove = true;
                    }
                }

            }

            var opacity = 1.1 - Math.abs((deltaX) / (this.deviceWidth / 2));
            if (this.horizontalMove) {
                event.preventDefault();
                event.stopPropagation();
                if (!this.moveOnNextFrame) {
                    this.moveOnNextFrame = true;

                    requestAnimationFrame(()=> {
                        if (this.refs.cards) {
                            this.moveOnNextFrame = false;
                            this.refs.cards.style.transform = "translateX(" + deltaX + "px)";
                            this.refs.cards.style.opacity = opacity;
                        }
                    });
                }
            }

        }
    },

    touchStart(event) {
        var touch = event.touches[0];
        this.startY = touch.pageY;
        this.startX = touch.pageX;
        this.deviceWidth = screen.width;
        // console.log("当前卡片的头部位置");
        this.props.recordingScrollTop();
        this.props.menuClose();
    },

    touchEnd(event) {
        var cards = this.refs.cards;
        this.curCardHeight = cards.offsetHeight;
        var touch = event.changedTouches[0];
        let absDeltaX = Math.abs(this.startX - touch.pageX);
        if (!this.verticalMove && !this.horizontalMove) {

            (this.props.display) ? this.props.cardShrink() : this.props.cardExpand();

        } else if (absDeltaX > this.deviceWidth / 2) {
            this.props.deleteCard();
        } else {
            if (this.horizontalMove) {
                requestAnimationFrame(()=> {
                    cards.style.transform = "translateX(" + 0 + "px)";
                    cards.style.opacity = 1;
                });
            }
        }
        this.horizontalMove = false;
        this.verticalMove = false;
    },

    goToQue(){
        this.setState({
            wordReview: true
        })
    },

    componentWillReceiveProps(nextProps){
        // console.log(nextProps.title, nextProps.display);
        if (!this.props.display && nextProps.display) {
            if (this.refs.videoPlayer) {
                // console.log('will receive play');
                if (this.refs.videoPlayer.paused) {
                    this.refs.videoPlayer.play();
                }
            }
        } else if (!nextProps.display) {
            if (this.refs.videoPlayer) {
                // console.log('will receive pause');
                this.refs.videoPlayer.pause();
            }
        }
    },

    showTest: function () {
        this.setState({
            videoEnd: true,
        });
    },

    skipVideo(){
        this.setState({
            videoEnd: true,
            videoSkip: true
        });
    },

    render(){
        var styles = {
            cardHeaderTitle: {
                paddingRight: "5px",
                fontSize: "0.8rem",
                wordWrap: "break-word",
            },
            floatingActionButton: {
                color: "blue"
            }
        };


        var src = {
            mp4: this.props.videoSrc
        };

        var divStyle = {
            transform: "translateY(" + this.props.transformDistance + ")"
            // top:this.props.transformDistance
        };


        var addTest = {
            "word": this.state.wordReview ?
                <WordQues sectionOver={this.props.sectionOver} videoSkip={this.state.videoSkip}
                          quesData={this.props.wordQues} addStudiedCount={this.props.addStudiedCount}
                          videoName={this.props.title}/> : <WordsTests
                words={this.props.words} goToQue={this.goToQue}/>,
            "reading": <ReadingTests sectionOver={this.props.sectionOver} readingTests={this.props.readingTests}
                                     changeCardPosition={this.props.changeCardPosition}/>,
            "listening": ""
        };

        var labelList = [];

        for (var i = 0; i < this.props.labels.length; i++) {
            if (i != (this.props.labels.length - 1)) {
                labelList.push([<span className="textMark"> {this.props.labels[i]} </span>, ","])
            } else {
                labelList.push([<span className="textMark"> {this.props.labels[i]} </span>])
            }
        }

        return (
            <div style={divStyle} className={this.props.animatedName} ref="cards">
                <Card>
                    <div className="card-header"
                         onTouchStart={this.touchStart}
                         onTouchEnd={this.touchEnd}
                         onTouchMove={this.touchMove}>
                        <CardHeader title={this.props.title} style={styles.cardHeader}
                                    textStyle={styles.cardHeaderTitle} titleStyle={styles.cardHeaderTitle}/>
                        <div className={this.props.display ? "hidden" : "card-text"}>
                            <div className="card-otherInfo">
                                <p className="cardType">{this.props.cardType == "reading" ? "阅读" : "单词"}</p>
                            </div>
                            <div className="cardDes">通过本视频你将学到关于
                                <span>
                                    {labelList}
                                </span>
                                方面的知识。 <span
                                    className="textMark"> {this.props.videoLength} </span></div>
                            {/*<Chip style={styles.chip} labelStyle={styles.chipLabel}>{this.props.cardType}</Chip>*/}
                            {/*<Chip style={styles.chip}>{this.props.videoLength}</Chip>*/}
                        </div>
                    </div>

                    <div className={this.props.display ? "card-media" : "hidden"}>
                        <div className="videoAndTest">
                            {this.state.videoEnd ? "" : <div
                                className={"tvCard" + " " + this.props.cardType + "Card"}>
                                <div className="tvCard-Video">
                                    <VideoPlayer src={src} shortCut={this.props.shortCut} ref="videoPlayer"
                                                 showTest={this.showTest}/>
                                </div>
                            </div>}
                            {this.state.videoEnd ? addTest[this.props.cardType] : ""}
                        </div>
                    </div>

                    <div className={(this.props.display && !this.state.videoEnd) ? "videoSkip" : "hidden"}
                         onClick={this.skipVideo}>
                        <FloatingActionButton mini={true} backgroundColor="#676264"><SkipNext/></FloatingActionButton>
                    </div>
                </Card>
            </div>
        )
    }
});

var Header = React.createClass({

    headerHeight: 0,


    componentDidMount: function () {
        this.headerHeight = this.refs.header.offsetHeight;
    },

    render() {
        return (
            <div className="header" ref="header">
                <AppBar
                    title="文勇课堂"
                    iconElementLeft={<Badge
                        badgeContent={this.props.studiedCount}
                        secondary={true}
                        badgeStyle={{top: 12, right: 12}}
                    />}
                    iconElementRight={
                        <IconMenu
                            iconButtonElement={
                                <IconButton onClick={this.props.menuOpen}><MoreVertIcon/></IconButton>
                            }
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                            open={this.props.menuState}>
                            {/*<MenuItem primaryText="Refresh" onTouchTap={this.menuClose}/>*/}
                            {/*<MenuItem primaryText="学习进度" onClick={this.props.menuClose}/>*/}
                            <MenuItem primaryText="查看排名" onClick={this.props.openRankBoard}/>
                            <MenuItem primaryText="更改主题" onClick={this.props.changeTheme}/>
                            <MenuItem primaryText="使用帮助" onClick={this.props.openHelp}/>
                            <MenuItem primaryText="添加留言" onClick={this.props.leaveMessage}/>
                            {/*<MenuItem primaryText="Sign out" onTouchTap={this.menuClose}/>*/}
                        </IconMenu>
                    }
                />

                <Snackbar open={this.props.showIncreasedTime} message={`又学习啦！厉害呀你`}/>
                <Snackbar open={this.props.showDeleteAlert} message="学习没有捷径,我还会再回来的!"/>
                <LinearProgress mode="determinate" value={this.props.timeProgress * 100 / (130 * 60)}
                                color={deepOrange300}/>
            </div>
        );
    }
});

var HelpFile = React.createClass({

    render(){

        const styles = {
            paper: {
                height: "80%",
                width: "90%",
                // margin: "0 auto",
                position: "relative",
                padding: "0.8rem",
                textAlign: 'left',
                display: 'inline-block',
                top: "4rem",
                zIndex: 2,
                fontSize: "0.8rem"
            },
            title: {
                fontSize: "1.2rem"
            },
            closeIcon: {
                position: "absolute",
                top: "0.2rem",
                right: "0.2rem"
            }
        };

        return (
            <div className={this.props.showHelpFile ? "helpFile" : "hidden"} onClick={this.props.menuClose}>
                <Paper style={styles.paper} zDepth={3}>
                    <IconButton style={styles.closeIcon} onClick={this.props.closeHelp}>
                        <CloseIcon/>
                    </IconButton>
                    <h3 className="styles.title">操作说明</h3>
                    <p>
                        知识点由一个个卡片构成,单击某卡片将出现该知识点的学习视频,视频结束后会出现相应练习题,做完所有题目您会看到本节学习结束按钮,单击该按钮则当前知识点学习结束。
                    </p>
                    <p>
                        如若对某一知识点不感兴趣,向左或向右滑动该卡片至完全透明可删除该卡片,系统将随后推荐相关内容助您掌握该知识点。
                    </p>
                </Paper>
                <div className="mask"></div>
            </div>
        )
    }

});

var RankingBoard = React.createClass({

    render(){
        const styles = {
            paper: {
                height: "80%",
                width: "90%",
                // margin: "0 auto",
                position: "relative",
                padding: "0.8rem",
                textAlign: 'left',
                display: 'inline-block',
                top: "4rem",
                zIndex: 2,
                fontSize: "0.8rem"
            },
            title: {
                fontSize: "1.2rem"
            },
            closeIcon: {
                position: "absolute",
                top: "0.2rem",
                right: "0.2rem"
            }
        };

        var remark="";
        if(this.props.rankBoardData){
            if(this.props.rankBoardData.haveStudied==0){
                remark = "千里之行，始于足下，同学快开始学习吧！"
            }else if(this.props.rankBoardData.rankNum==1){
                remark = "厉害！继续保持。"
            }else if(this.props.rankBoardData.prevDis==0){
                remark = "和前一名看了一样多的视频，不过他完成的比你快。再做一个题目就可以超过他啦！"
            }else if(this.props.rankBoardData.prevDis<=3){
                remark = `偷偷告诉您，您和前一名仅差${this.props.rankBoardData.prevDis}个视频。`
            }else{
                remark = `和前一名还差${this.props.rankBoardData.prevDis}个视频，加油加油！`
            }
        }
        console.log("remark");
        console.log(remark);

        return (
            <div className={this.props.showRankBoard ? "rankBoard" : "hidden"} onClick={this.props.menuClose}>
                <Paper style={styles.paper} zDepth={3}>
                    <IconButton style={styles.closeIcon} onClick={this.props.closeRankBoard}>
                        <CloseIcon/>
                    </IconButton>
                    <img src={this.props.rankBoardData.headImg ? this.props.rankBoardData.headImg : ""} alt=""
                         className="headImg"/>
                    <h3 className="userName">{this.props.rankBoardData.nikeName ? this.props.rankBoardData.nikeName : ""}</h3>
                    <p className="rank">您当前是第 <span className="curRank">{this.props.rankBoardData.rankNum ? this.props.rankBoardData.rankNum : ""}</span>名</p>
                    <p className="remark">{remark}</p>
                </Paper>
                <div className="mask"></div>
            </div>
        )
    }

});

module.exports = MaterialCards;