import React, {Component} from 'react';
import Done from 'react-icons/lib/md/done';
import FlatButton from 'material-ui/FlatButton';
import {request} from '../../../lib';

require('./WordQues.scss');

class WordQues extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            optClickState: [],
            testOver: false,

        });
        this.answerList = [];
        this.pass = false;
        this.clickTime = 0;

        this.optClick = this.optClick.bind(this);
        this.sectionOver = this.sectionOver.bind(this);
        this.testOver = this.testOver.bind(this);
    }

    optClick(i) {
        // console.log(i);
        this.state.optClickState[i] = true;
        if (this.clickTime == 0 && this.state.optClickState[i] && this.props.quesData.options[i].testOptState) {
            this.pass = true;
            console.log("pass");
        }

        this.clickTime++;
        this.answerList.push(this.props.quesData.options[i].testOptContent);
        if (!this.state.testOver) {
            this.state.testOver = this.state.optClickState[i] && this.props.quesData.options[i].testOptState;
        }
        this.setState({});
    }

    testOver(){
        this.setState({
            testOver:true
        })
    }

    sectionOver() {
        var curTime = new Date;
        var body = {};
        body.videoName = this.props.videoName;
        body.testUniId = this.props.quesData.testUniId;
        body.curTime = curTime;
        body.answerList = this.answerList;
        body.videoSkip = this.props.videoSkip;
        body.pass = this.pass;
        body.queType = this.props.quesData.queType;

        if ((body.pass && !body.videoSkip) || (!body.videoSkip && (body.queType == 'trans'))) {
            console.log("addCount");
            this.props.addStudiedCount();
            request('/userAction/sectionOver', body);
        }
        // console.log(body);

        this.props.sectionOver()
    }

    render() {

        return (
            <div className="wordQues">
                {this.props.quesData.queType == "trans" ? <TransQue transContent={this.props.quesData.transContent}
                                                                    transResult={this.props.quesData.transResult}
                                                                    testOver={this.testOver}/> :
                    <WordQue testStem={this.props.quesData.testStem}
                             testTitle={this.props.quesData.testTitle}
                             testWord={this.props.quesData.testWord}
                             correctAnswer={this.props.quesData.correctAnswer}
                             options={this.props.quesData.options}
                             optClickState={this.state.optClickState}
                             optClick={this.optClick}
                    />}

                <div className={this.state.testOver ? "testOver" : "hidden"} onClick={this.sectionOver}>
                    <FlatButton label="学习结束"/>
                </div>

            </div>
        )
    }
}

class WordQue extends Component {
    constructor(props) {
        super(props);
        this.optHaveClick = this.optHaveClick.bind(this)
    }

    optHaveClick() {
        console.log("此选项已选")
    }

    render() {

        return (
            <div className="wordQue">
                <div className="wordQueStem" dangerouslySetInnerHTML={{__html: this.props.testStem}}>
                </div>

                <div className="wordQueTitle">
                    {this.props.testTitle}
                </div>

                {this.props.options.map((opt, index)=> {
                    return (
                        <div
                            className={this.props.optClickState[index] && !opt.testOptState ? "wordQueOpt answerWrong" : "wordQueOpt"}
                            key={`opt${index}`}
                            onClick={this.props.optClickState[index] ? this.optHaveClick : this.props.optClick.bind(this, index)}>

                            <span
                                className={opt.testOptState && this.props.optClickState[index] ? "optContent optRight" : "optContent"}>{opt.testOptContent}</span>

                            {opt.testOptState && this.props.optClickState[index] ?
                                <span className="answerRight"><Done/></span> : ""}

                        </div>
                    )
                })}
            </div>
        )
    }
}

class TransQue extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            showResult: false
        });
        this.showResult = this.showResult.bind(this);
    }

    showResult() {
        this.props.testOver();
        this.setState({
            showResult: true
        })
    }

    render() {

        return (
            <div className="transQue">
                <div className="transContent" dangerouslySetInnerHTML={{__html: this.props.transContent}}>
                </div>
                <div className={this.state.showResult ? "transResult" : "hidden"}
                     dangerouslySetInnerHTML={{__html: this.props.transResult}}>
                </div>
                <div className={this.state.showResult?"hidden":"showResultBtn"} >
                    <FlatButton label="显示翻译" onClick={this.showResult}/>
                </div>
            </div>
        )
    }
}

//解析
//{/*<span*/}
//    {/*className={this.props.optClickState[index] ? "optExplain" : "hidden"}>{opt.testOptExplain}</span>*/}
//TO USE
//<span className={this.props.hasClick ? "optExplain" : "hidden"}>{opt.optExplain}</span>
//{this.props.answerState ? <Done/> : ""}

module.exports = WordQues;