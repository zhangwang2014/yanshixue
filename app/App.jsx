import React from 'react';
import {IndexLink, Link} from 'react-router';
import LocalLibrary from 'react-icons/lib/md/local-library';
import ChromeReader from 'react-icons/lib/md/chrome-reader-mode';
import Search from 'react-icons/lib/md/search';
import Account from 'react-icons/lib/md/account-circle';

require('./App.scss');
class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="app">
                <ul className="weui-tabbar">
                    <li className="weui-tabbar__item">
                        <Link to="/fe/cards" activeStyle={{color: '#09bb07'}}
                                   style={{color: '#999999'}}>
                            <LocalLibrary className="weui-tabbar__icon"/>
                            <p className="tabbar_label">我的课堂</p>
                        </Link>
                    </li>
                    <li className="weui-tabbar__item">
                        <Link to='/fe/strengthen' activeStyle={{color: '#09bb07'}}
                              style={{color: '#999999'}}>
                            <ChromeReader className="weui-tabbar__icon"/>
                            <p className="tabbar_label">知识点强化</p>
                        </Link>
                    </li>
                    <li className="weui-tabbar__item">
                        <Link to='/fe/search' activeStyle={{color: '#09bb07'}}
                              style={{color: '#999999'}}>
                            <Search className="weui-tabbar__icon"/>
                            <p className="tabbar_label">资料搜索</p>
                        </Link>
                    </li>
                    <li className="weui-tabbar__item">
                        <Link to='/fe/userInfo' activeStyle={{color: '#09bb07'}}
                              style={{color: '#999999'}}>
                            <Account className="weui-tabbar__icon"/>
                            <p className="tabbar_label">学习记录</p>
                        </Link>
                    </li>
                </ul>
                {this.props.children}
            </div>
        )
    }
}

export default App;