var OAuth = require('wechat-oauth');
// var appId = 'wx56f6bf06d96a95a5';
// var appSecret = '9a571fd12c0f34b2e466a2e99aa6aa1e';
var appId = 'wxa5b45a1c0462d284';
var appSecret = '92bf7dd863d641f6dead3680982f6bce';
var fs = require('fs');

// var oauthApi = new OAuth(appId, appSecret, function (openid, callback) {
//     // 传入一个根据openid获取对应的全局token的方法
//     // 在getUser时会通过该方法来获取token
//     fs.readFile(`./oauth/${openid}:access_token.json`, 'utf8', function (err, txt) {
//         if (err) {
//             return callback(err);
//         }
//         callback(null, JSON.parse(txt));
//     });
// }, function (openid, token, callback) {
//     // 请将token存储到全局，跨进程、跨机器级别的全局，比如写到数据库、redis等
//     // 这样才能在cluster模式及多机情况下使用，以下为写入到文件的示例
//     // 持久化时请注意，每个openid都对应一个唯一的token!
//     fs.writeFile(`./oauth/${openid}:access_token.json`, JSON.stringify(token), callback);
// });

var oauthApi =new OAuth(appId, appSecret);

//var code = '00163f80b2d92b4f5a3fc9878224cab1';
//oauthApi.getAccessToken(code, function (err, result) {
//    if (err) {
//        console.log(err);
//    } else {
//        var accessToken = result.data.access_token;
//        var openid = result.data.openid;
//    }
//});

//oauthApi.getUser('ol56is1CRIb3T5Vhl7SgOLR4V9K8', function (err, result) {
//    var userInfo = result;
//    console.log(userInfo);
//});

module.exports = {
    getUrl: (redirectUrl)=> oauthApi.getAuthorizeURL('http://daydayupinc.com/u/oauth' + (redirectUrl && ('?redirectUrl=' + encodeURIComponent(redirectUrl))), '0215', 'snsapi_userinfo'),
    getAccessToken: oauthApi.getAccessToken.bind(oauthApi),
    getUser: oauthApi.getUser.bind(oauthApi)
};

//console.log(module.exports.getUrl());