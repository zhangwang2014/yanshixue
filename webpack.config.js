var webpack = require('webpack');
var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

var isProduction = function () {
    return process.env.NODE_ENV === 'production';
};

var entry = './app/index.js';
var outputPath = './dist';

var plugins = [
    new webpack.optimize.CommonsChunkPlugin({
        name: 'commons',
        filename: 'js/commons.js'
    }, {
        name: 'vendor',
        filename: 'js/vendor.bundle.js'
    }),
    new webpack.ProvidePlugin({
        'Promise': 'exports?global.Promise!es6-promise',
        'fetch': 'exports?self.fetch!whatwg-fetch'
    }),
    new HtmlWebpackPlugin({
        filename: 'index.html',
        template: 'app/index.html',
        inject: 'body',
    }),
    new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false
        }
    })
];
if (isProduction()) {
    plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            test: /(\.jsx|\.js)$/,
            compress: {
                warnings: false
            },
        })
    );
}

var config = {
    target: 'web',
    cache: true,
    entry: {
        app: entry,
        vendor: ["react", "react-dom","antd"],

    },
    output: {
        path: path.join(__dirname, outputPath),
        filename: 'js/[name].bundle.js',
        chunkFilename: '[name].chunk.js',
        publicPath: '/dist/'
    },
    module: {
        loaders: [{
            test: /(\.jsx|\.js)$/,
            loader: 'babel?presets[]=es2015&presets[]=react',
            exclude: /node_modules/
        },
            {
                test: /\.scss$/,
                loaders: ['style', 'css', 'sass'],
            },
            {
                test: /\.less$/,
                loaders: ['style', 'css', 'less'],
            },
            {
                test: /\.css$/,
                loaders: ['style', 'css'],
            },
            {
                test: /\.json$/,
                loader: 'json',
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                loader: 'url?limit=8024&name=images/[name].[ext]'
            },
            {
                test: /\.(woff2?|otf|eot|svg|ttf)$/i,
                loader: 'url?name=fonts/[name].[ext]'
            },
            {
                test: /\.html$/,
                loader: 'url?name=[name].[ext]'
            },
        ],
    },
    plugins: plugins,
    resolve: {
        root: __dirname,
        extensions: ['', '.js', '.jsx']
    },
    devtool: 'null',
};

module.exports = config;