var express = require('express');
var router = express.Router();
var path = require('path');
var oauth = require('../wechatOauth');
var config = require('../config.js');

// var Sequelize = require('sequelize');
var sequelize = require('../db.js');
var UserInfo = sequelize.UserInfo;
var UserData = sequelize.UserData;

//router.use('*', (req, res, next)=> {
//    console.log(req);
//    console.log(req.originalUrl);
//    next();
//});

router.get('/', (req, res, next) => {
    //res.cookie('authorized', true);
    res.send({});
    // console.log("微信登录验证4");
    //res.redirect('/');
    //res.send((function anonymous(it /**/) {
    //    var out = '<div>Nickname: ' + (it.nickname) + '</div><div>Sex: ' + (it.sex) + '</div><div>Avatar: <img style="width: 100px; height: 100px;" src="' + (it.headimgurl) + '"/></div>';
    //    return out;
    //})(req.session.userInfo));
});

//passing oauth as a parameter
// router.use('/sms', require('./sms'));

function cbWrapper(cb) {
    return (err, result)=> {
        if (err) {
            console.log(err);
        } else cb(result);
    };
}

router.get('/oauth', (req, res, next)=> {
    oauth.getAccessToken(req.query.code, cbWrapper((result)=> {
        var openid = result.data.openid;
        // console.log('微信登录验证5');
        // console.log("openid");
        // console.log(openid);
        // console.log("openid" + openid);
        // req.session.wxAuthorized = true;
        // res.cookie('authorized', true, {maxAge: config.sessionMaxAge});
        //判断session中是否存有该用户的unionid
        //不存在则首先在数据库中检测该用户是否存在
        oauth.getUser(openid, cbWrapper((userInfo)=> {
            // console.log(userInfo);
            UserInfo.findOne({where: {openid: userInfo.openid}}).then(function (userExist) {
                if (userExist) {
                    // req.session.unionid = userInfo.unionid;
                    // console.log(req.session);
                    console.log(userExist);
                    console.log("session中不存在,数据库中已存在")
                } else {
                    UserInfo.create({
                        unionid: userInfo.unionid,
                        openid: userInfo.openid,
                        nikeName: userInfo.nickname,
                        sex: userInfo.sex,
                        language: userInfo.language,
                        city: userInfo.city,
                        province: userInfo.province,
                        country: userInfo.country,
                        headimgurl: userInfo.headimgurl,
                        privilege: userInfo.privilege
                    });

                    UserData.create({
                        unionid: userInfo.unionid,
                        openid: userInfo.openid
                    });

                    // req.session.unionid = userInfo.unionid;
                    // console.log(req.session);
                    console.log("用户不存在,新建用户:" + userInfo.nickname)
                }
            }, function (err) {
                console.log(err);
            });
        }));
        res.redirect('/?openid=' + openid);

    }));
});

module.exports = router;
