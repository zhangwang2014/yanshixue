var express = require('express');
var path = require('path');
var fs = require('fs');
var router = require('express').Router();

router.use('/upLoad', require('./routes/upLoad'));

router.use('/saveData', require('./routes/saveData'));

router.use('/readData', require('./routes/readData'));

router.use('/deleteData', require('./routes/deleteData'));

router.use('/dashBoard',require('./routes/dashBoard'));

router.get("*", function (req, res) {
    res.sendFile(path.resolve(__dirname, '../dist/index.html'));
});

module.exports=router;