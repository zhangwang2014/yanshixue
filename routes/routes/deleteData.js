var router = require('express').Router();

var sequelize = require('../../db');
var WordData = sequelize.WordData;
var AllData = sequelize.AllData;

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

router.post("/single", jsonParser, function (req, res, err) {
    WordData.destroy({
        where: {
            videoName: req.body.videoName
        }
    }).then(
        AllData.destroy({
            where: {
                videoName: req.body.videoName
            }
        })
    ).then(
        AllData.findAll().then(function (allData) {
            // console.log(allData);
            var dataSource = [];
            for (var i = 0; i < allData.length; i++) {
                dataSource[i] = {};
                dataSource[i].index = i + 1;
                dataSource[i].name = allData[i].dataValues.videoName;
                dataSource[i].category = allData[i].dataValues.type;
                dataSource[i].videoLength = allData[i].dataValues.videoLength;
                dataSource[i].viewingTimes = allData[i].dataValues.viewingTimes;
                dataSource[i].label1 = allData[i].dataValues.labels[0];
                dataSource[i].label2 = allData[i].dataValues.labels[1];
                dataSource[i].label3 = allData[i].dataValues.labels[2];
            }
            res.send({dataSource: dataSource});
            // console.log(dataSource);
        })
    )
});

module.exports = router;