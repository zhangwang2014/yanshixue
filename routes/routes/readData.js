var router = require('express').Router();

var sequelize = require('../../db');
var WordData = sequelize.WordData;
var WordTests = sequelize.WordTests;
var AllData = sequelize.AllData;

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

router.post("/all", jsonParser, function (req, res, err) {
    AllData.findAll().then(function (allData) {
        // console.log(allData);
        var dataSource = [];
        for (var i = 0; i < allData.length; i++) {
            dataSource[i] = {};
            dataSource[i].index = i + 1;
            dataSource[i].name = allData[i].dataValues.videoName;
            dataSource[i].category = allData[i].dataValues.type;
            dataSource[i].videoLength = allData[i].dataValues.videoLength;
            dataSource[i].viewingTimes = allData[i].dataValues.viewingTimes;
            dataSource[i].label1 = allData[i].dataValues.labels[0];
            dataSource[i].label2 = allData[i].dataValues.labels[1];
            dataSource[i].label3 = allData[i].dataValues.labels[2];
        }
        res.send({dataSource: dataSource});
        // console.log(dataSource);
    })
});

router.post("/word", jsonParser, function (req, res, err) {
    AllData.findAll({
        where: {
            type: "word"
        }
    }).then(function (allData) {
        // console.log(allData);
        var dataSource = [];
        for (var i = 0; i < allData.length; i++) {
            dataSource[i] = {};
            dataSource[i].index = i + 1;
            dataSource[i].name = allData[i].dataValues.videoName;
            dataSource[i].category = allData[i].dataValues.type;
            dataSource[i].videoLength = allData[i].dataValues.videoLength;
            dataSource[i].viewingTimes = allData[i].dataValues.viewingTimes;
            dataSource[i].label1 = allData[i].dataValues.labels[0];
            dataSource[i].label2 = allData[i].dataValues.labels[1];
            dataSource[i].label3 = allData[i].dataValues.labels[2];
        }
        res.send({dataSource: dataSource});
        // console.log(dataSource);
    })
});

router.post("/singleData", jsonParser, function (req, res, err) {
    var oldWordData = {};
    var sendData = true;
    console.log(req.body.videoName);
    WordData.findOne({
        where: {
            videoName: req.body.videoName
        }
    }).then(function (wordData) {
        // console.log(wordData);
        oldWordData.videoName = wordData.videoName;
        oldWordData.labels = wordData.labels;
        oldWordData.videoFeature = wordData.videoFeature;
        oldWordData.prevWord = wordData.prevWord;
        oldWordData.nextWord = wordData.nextWord;
        oldWordData.testsWords = wordData.testsWords;
        oldWordData.remarks = wordData.remarks;
        oldWordData.videoUrl = wordData.videoUrl;
        oldWordData.wordQues = wordData.wordQues;
        oldWordData.testData = [1];
        console.log("oldWordData");
        console.log(oldWordData);
        if (wordData.wordQues) {
            wordData.wordQues.map(function (queId, index) {
                // console.log(index);
                WordTests.findById(queId).then(function (wordTest) {
                    // console.log("~~~~~~~~~~~");
                    // console.log(wordTest);
                    // console.log("~~~~~~~~~~~");
                    if (wordTest) {
                        if (wordTest.queType == "trans") {
                            // console.log(wordTest);
                            var transQue = {};
                            transQue.testWord = wordTest.testWord;
                            transQue.transContent = wordTest.transContent;
                            transQue.transResult = wordTest.transResult;
                            transQue.queType = "trans";
                            transQue.testUniId = queId;
                            console.log("读取的翻译数据");
                            console.log(transQue);
                            oldWordData.testData.push(transQue);
                        } else {
                            var oneQue = {};
                            oneQue.testWord = wordTest.testWord;
                            oneQue.correctAnswer = wordTest.correctAnswer;
                            oneQue.testStem = wordTest.testStem;
                            oneQue.testTitle = wordTest.testTitle;
                            oneQue.options = wordTest.options;
                            oneQue.remarks = wordTest.remarks;
                            oneQue.testUniId = queId;
                            oldWordData.testData.push(oneQue);
                        }
                        // console.log(oneQue);
                    }
                }).then(function () {
                    if (index == (wordData.wordQues.length - 1) && sendData) {
                        sendData = false;
                        console.log("最终发送的数据");
                        console.log(oldWordData);
                        res.send({oldWordData: oldWordData});
                    }
                })
            }, this)
        } else {
            res.send({oldWordData: oldWordData});
        }
    })
});

module.exports = router;