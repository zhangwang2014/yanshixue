var router = require('express').Router();
// var execSync = require('execSync');

//TODO 查询multer中间件和node crypo的相关知识
var multer = require('multer');
var crypto = require('crypto');
var config = require('../../config');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __dirname + '/upload')
    },

    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            cb(err, err ? undefined : raw.toString('hex') + '_' + file.originalname);
        });
    }
});

var upload = multer({
    storage: storage,
    // fileFilter: fileFilter,
    limits: {
        fileSize: 10 * 1024 * 1024 * 20
    }
});

// router.post('/test', upload.single('file'), function (req, res, next) {
//     console.log(req.file);
//     console.log(req.body);
//     console.log("req.file");
//     res.send({});
// });

router.post("/fileUpLoad", upload.single('file'), function (req, res, err) {
    // console.log(req.file);
    var qiniu = require("qiniu");
    //需要填写你的 Access Key 和 Secret Key
    qiniu.conf.ACCESS_KEY = config.qiNiu.Access_Key;
    qiniu.conf.SECRET_KEY = config.qiNiu.Secret_Key;

    //要上传的空间
    var bucket = config.qiNiu.bucket;

    //上传到七牛后保存的文件名
    var videoFormat = req.file.originalname.slice(req.file.originalname.lastIndexOf('.'), req.file.originalname.length);
    console.log("format");
    console.log(videoFormat);

    if (videoFormat == ".mp4") {
        key = req.body.type + "/" + req.file.originalname;
        console.log(".mp4");
        // console.log(key);
    } else if (videoFormat == ".trec") {
        key = "ori/" + req.body.type + "/" + req.file.originalname;
        console.log(".trec");
        // console.log(key);
    }

    var pipeline = 'YanShiXueYuan';
    var fopsVideo = 'avthumb/mp4/ab/256k/ar/44100/acodec/libfaac';
    var fopsImg = 'vframe/jpg/offset/1/w/480/h/480';

    var saveas_keyVideo = qiniu.util.urlsafeBase64Encode(`${bucket}:trans/${req.body.type}/${req.file.originalname}`);
    var saveas_keyImg = qiniu.util.urlsafeBase64Encode(`${bucket}:shortcut/${req.body.type}/${req.file.originalname}`);

    // console.log(saveas_keyVideo);
    // console.log(saveas_keyImg);

    fopsVideo = fopsVideo + '|saveas/' + saveas_keyVideo;
    fopsImg = fopsImg + '|saveas/' + saveas_keyImg;
    // console.log(fopsVideo);
    // console.log(fopsImg);

    //构建上传策略函数
    function oriUptoken(bucket, key) {
        var putPolicy = new qiniu.rs.PutPolicy(bucket + ":" + key);
        return putPolicy.token();
    }

    function transUptoken(bucket, key) {
        var putPolicy = new qiniu.rs.PutPolicy(bucket + ":" + key);
        putPolicy.persistentOps = fopsVideo;
        putPolicy.persistentPipeline = pipeline;
        return putPolicy.token();
    }

    function ImgUptoken(bucket, key) {
        var putPolicy = new qiniu.rs.PutPolicy(bucket + ":" + key);
        putPolicy.persistentOps = fopsImg;
        putPolicy.persistentPipeline = pipeline;
        return putPolicy.token();
    }


    var filePath = req.file.path;
    //生成上传 Token

    var uptokenOri = oriUptoken(bucket, key);
    var uptokenTrans = transUptoken(bucket, key);
    var uptokenImg = ImgUptoken(bucket, key);


    //转码是使用的队列名称。 #设定自己账号下的pipleline
    // pipeline = 'abc';

    //构造上传函数
    function uploadFile(uptoken, key, localFile) {
        var extra = new qiniu.io.PutExtra();
        qiniu.io.putFile(uptoken, key, localFile, extra, function (err, ret) {
            if (!err) {
                // 上传成功， 处理返回值
                console.log(ret.hash, ret.key, ret.persistentId);
            } else {
                // 上传失败， 处理返回代码
                console.log(err);
            }
        });
    }

    // var duration = parseFloat(execSync(`ffprobe -i "${filePath}" -show_entries format=duration -v quiet -of csv="p=0"`));
    // console.log(duration);

    //调用uploadFile上传
    if (videoFormat == ".mp4") {
        uploadFile(uptokenTrans, key, filePath);
        uploadFile(uptokenImg, key, filePath);
    } else if (videoFormat == ".trec") {
        uploadFile(uptokenOri, key, filePath);
    }
    res.send({});

});

module.exports = router;