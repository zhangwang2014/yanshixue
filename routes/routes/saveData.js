var router = require('express').Router();

var sequelize = require('../../db');
var WordData = sequelize.WordData;
var WordTests = sequelize.WordTests;
var AllData = sequelize.AllData;

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();


router.post("/saveWord", jsonParser, function (req, res, err) {
    var testData = req.body.testData;
    // wordQues用户获取单词测试题在单词测试题数据库中的编号

    var wordQues = [];
    var saveWord = true;

    testData.map(function (value, index) {
        // console.log(value);
        if (index > 0) {
            WordTests.findById(value.testUniId).then(function (wordTest) {
                if (wordTest) {
                    console.log("存在");
                    if (wordTest.queType == "trans") {
                        wordTest.testWord = value.testWord;
                        wordTest.transContent = value.transContent;
                        wordTest.transResult = value.transResult;
                        wordQues.push(value.testUniId);
                    } else {
                        wordTest.testWord = value.testWord;
                        wordTest.correctAnswer = value.correctAnswer;
                        wordTest.testStem = value.testStem;
                        wordTest.testTitle = value.testTitle;
                        wordTest.options = value.options;
                        wordTest.changed('options', true);
                        wordTest.remarks = value.remarks;
                        wordTest.testUniId = value.testUniId;
                        wordQues.push(value.testUniId);
                        wordTest.save();
                    }
                    if ((wordQues.length == testData.length - 1) && saveWord) {
                        saveWord = false;
                        WordData.findOne({where: {videoName: req.body.videoName}}).then(function (wordData) {
                            if (wordData) {
                                // wordData.videoName = req.body.videoName;

                                wordData.labels = req.body.labels;
                                // console.log(req.body.labels);
                                wordData.changed('labels', true);
                                wordData.videoFeature = req.body.videoFeature;
                                wordData.prevWord = req.body.prevWord;
                                wordData.nextWord = req.body.nextWord;
                                wordData.testsWords = req.body.testsWords;
                                wordData.changed('testsWords', true);
                                wordData.remarks = req.body.remarks;
                                wordData.videoUrl = req.body.videoUrl;
                                wordData.oriVideoUrl = req.body.oriVideoUrl;
                                wordData.transVideoUrl = req.body.transVideoUrl;
                                wordData.videoShortCut = req.body.videoShortCut;
                                wordData.wordQues = wordQues;
                                wordData.changed('wordQues', true);
                                wordData.save();
                            } else {
                                WordData.create({
                                    videoName: req.body.videoName,
                                    labels: req.body.labels,
                                    videoFeature: req.body.videoFeature,
                                    prevWord: req.body.prevWord,
                                    nextWord: req.body.nextWord,
                                    testsWords: req.body.testsWords,
                                    remarks: req.body.remarks,
                                    videoUrl: req.body.videoUrl,
                                    oriVideoUrl: req.body.oriVideoUrl,
                                    transVideoUrl: req.body.transVideoUrl,
                                    videoShortCut: req.body.videoShortCut,
                                    wordQues: wordQues
                                })
                            }
                        });
                    }
                } else {
                    console.log("不存在");
                    if (value.queType == 'trans') {
                        console.log('保存翻译题');
                        console.log(value);
                        WordTests.create({
                            testWord: value.testWord,
                            transContent: value.transContent,
                            transResult: value.transResult,
                            queType: value.queType
                        }).then(function () {
                            console.log('保存翻译题2');
                            WordTests.findOne({
                                where: {
                                    transContent: value.transContent,
                                }
                            }).then(function (wordTest) {
                                console.log('翻译题的数据');
                                console.log(wordTest);
                                console.log("上面是翻译题的数据");
                                wordQues.push(wordTest.id)
                            }).then(function () {
                                if ((wordQues.length == testData.length - 1) && saveWord) {
                                    saveWord = false;
                                    WordData.findOne({where: {videoName: req.body.videoName}}).then(function (wordData) {
                                        if (wordData) {
                                            wordData.labels = req.body.labels;
                                            // console.log(req.body.labels);
                                            wordData.changed('labels', true);
                                            wordData.videoFeature = req.body.videoFeature;
                                            wordData.prevWord = req.body.prevWord;
                                            wordData.nextWord = req.body.nextWord;
                                            wordData.testsWords = req.body.testsWords;
                                            wordData.changed('testsWords', true);
                                            wordData.remarks = req.body.remarks;
                                            wordData.videoUrl = req.body.videoUrl;
                                            wordData.oriVideoUrl = req.body.oriVideoUrl;
                                            wordData.transVideoUrl = req.body.transVideoUrl;
                                            wordData.videoShortCut = req.body.videoShortCut;
                                            wordData.wordQues = wordQues;
                                            wordData.changed('wordQues', true);
                                            wordData.save();
                                        } else {
                                            WordData.create({
                                                videoName: req.body.videoName,
                                                labels: req.body.labels,
                                                videoFeature: req.body.videoFeature,
                                                prevWord: req.body.prevWord,
                                                nextWord: req.body.nextWord,
                                                testsWords: req.body.testsWords,
                                                remarks: req.body.remarks,
                                                videoUrl: req.body.videoUrl,
                                                oriVideoUrl: req.body.oriVideoUrl,
                                                transVideoUrl: req.body.transVideoUrl,
                                                videoShortCut: req.body.videoShortCut,
                                                wordQues: wordQues
                                            })
                                        }
                                    });
                                }
                            })
                        })
                    } else {
                        WordTests.create({
                            testWord: value.testWord,
                            correctAnswer: value.correctAnswer,
                            testStem: value.testStem,
                            testTitle: value.testTitle,
                            options: value.options,
                            remarks: value.remarks
                        }).then(function () {
                            WordTests.findOne({
                                where: {
                                    testWord: value.testWord,
                                    testStem: value.testStem
                                }
                            }).then(function (wordTest) {
                                // console.log("创建了");
                                // console.log(wordTest.id);
                                wordQues.push(wordTest.id);
                            }).then(function () {
                                if ((wordQues.length == testData.length - 1) && saveWord) {
                                    saveWord = false;

                                    WordData.findOne({where: {videoName: req.body.videoName}}).then(function (wordData) {
                                        if (wordData) {

                                            // wordData.videoName = req.body.videoName;

                                            wordData.labels = req.body.labels;
                                            // console.log(req.body.labels);
                                            wordData.changed('labels', true);
                                            wordData.videoFeature = req.body.videoFeature;
                                            wordData.prevWord = req.body.prevWord;
                                            wordData.nextWord = req.body.nextWord;
                                            wordData.testsWords = req.body.testsWords;
                                            wordData.changed('testsWords', true);
                                            wordData.remarks = req.body.remarks;
                                            wordData.videoUrl = req.body.videoUrl;
                                            wordData.oriVideoUrl = req.body.oriVideoUrl;
                                            wordData.transVideoUrl = req.body.transVideoUrl;
                                            wordData.videoShortCut = req.body.videoShortCut;
                                            wordData.wordQues = wordQues;
                                            wordData.changed('wordQues', true);
                                            wordData.save();
                                        } else {
                                            WordData.create({
                                                videoName: req.body.videoName,
                                                labels: req.body.labels,
                                                videoFeature: req.body.videoFeature,
                                                prevWord: req.body.prevWord,
                                                nextWord: req.body.nextWord,
                                                testsWords: req.body.testsWords,
                                                remarks: req.body.remarks,
                                                videoUrl: req.body.videoUrl,
                                                oriVideoUrl: req.body.oriVideoUrl,
                                                transVideoUrl: req.body.transVideoUrl,
                                                videoShortCut: req.body.videoShortCut,
                                                wordQues: wordQues
                                            })
                                        }
                                    });
                                }
                            })
                        })
                    }
                }
            })
        }
    }, this)
    // console.log(testData);


    AllData.findOne({where: {videoName: req.body.videoName}}).then(function (allData) {
        if (allData) {
            allData.videoName = req.body.videoName;
            allData.type = "word";
            allData.labels = req.body.labels;
            allData.changed('testsWords', true);
            allData.save();
        } else {
            AllData.create({
                videoName: req.body.videoName,
                type: "word",
                labels: req.body.labels,
                viewingTimes: 0
            })
        }
        res.send({success: true});
    });
});

module.exports = router;