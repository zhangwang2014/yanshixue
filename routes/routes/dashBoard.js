var router = require('express').Router();

var sequelize = require('../../db');
var UserData = sequelize.UserData;
var UserInfo = sequelize.UserInfo;

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

router.post("/rankAllUser", jsonParser, function(req, res, err) {

    var allUserRankData = [];


    function compare(a, b) {
        if (a.studied.length > b.studied.length) {
            return -1;
        } else if (a.studied.length < b.studied.length) {
            return 1;
        } else if (a.studied.length == 0 && b.studied.length == 0) {
            return 0;
        } else {
            var alastLearningTime = a.studied[a.studied.length - 1].passTime;
            var blastLearningTime = b.studied[b.studied.length - 1].passTime;
            if (alastLearningTime > blastLearningTime) {
                return 1;
            } else {
                return -1;
            }
        }
    }


    UserData.findAll().then(function(userData) {
        if (userData) {
            userData.map((curValue, index) => {
                allUserRankData[index] = {};
                allUserRankData[index].openId = curValue.openid;
                allUserRankData[index].studied = curValue.studied;

                if(!allUserRankData[index].studied){
                    allUserRankData[index].studied=[]
                }

                learningRecord = curValue.learningRecord;
                // console.log(learningRecord);
                if (learningRecord) {
                    var corCount = 0;

                    for (var i = 0; i < learningRecord.length;i++) {
                        // console.log(learningRecord[i].answerLists.length);

                        if(learningRecord[i].answerLists && learningRecord[i].answerLists.length==1){
                            corCount++;
                            console.log("abc");
                        }
                    }

                allUserRankData[index].correctRate = (corCount/learningRecord.length)*100;
                } else {
                    allUserRankData[index].correctRate = null;
                }
            })
            allUserRankData.sort(compare);
            // res.send({ allUserRankData: allUserRankData });
        }

    }).then(function() {
        UserInfo.findAll().then(function(userInfo) {
            userInfo.map((curValue, index) => {

                // console.log(curValue.nikeName);
                for (var k = 0; k < allUserRankData.length; k++) {
                    if (allUserRankData[k].openId == curValue.openid) {
                        allUserRankData[k].nikeName = curValue.nikeName;
                        allUserRankData[k].headImg = curValue.headimgurl;
                        break;
                    }
                }

            })
            res.send({ allUserRankData: allUserRankData })
        })
    })
});

module.exports = router;
