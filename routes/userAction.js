var express = require('express');
var path = require('path');
var fs = require('fs');
var router = require('express').Router();

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

// router.use('/sectionOver', require('./userAction/sectionOver'));

// router.use('/ranking', require('./userAction/ranking'));
var sequelize = require('../db');
var AllData = sequelize.AllData;
var WordTests = sequelize.WordTests;
var UserData = sequelize.UserData;
var UserInfo = sequelize.UserInfo;

router.post("/sectionOver", jsonParser, function (req, res, err) {
    // console.log(req.body);

    UserData.findOne({where: {openid: req.session.openid}}).then(function (userData) {
        var everDone = false;

        if (!userData.studied) {
            userData.studied = []
        }
        if ((req.body.pass && !req.body.videoSkip) || (!req.body.videoSkip && (req.body.queType == 'trans'))) {

            userData.studied.push({
                videoName: req.body.videoName,
                testUniId: req.body.testUniId,
                passTime: req.body.curTime
            });
            userData.changed('studied', true);
        }

        if (req.body.pass && !req.body.videoSkip) {
            if (!userData.learningRecord) {
                userData.learningRecord = []
            }
            userData.learningRecord.forEach(function (learningRecord, index) {
                if (learningRecord.videoName == req.body.videoName && learningRecord.testUniId == req.body.testUniId) {
                    userData.learningRecord[index].answerLists.push(req.body.answerList);
                    userData.learningRecord[index].videoWatchingTimes++;
                    userData.changed('learningRecord', true);
                    everDone = true
                }
            });

            if (!everDone) {
                userData.learningRecord.push({
                    videoName: req.body.videoName,
                    testUniId: req.body.testUniId,
                    testTime: req.body.curTime,
                    answerLists: [req.body.answerList],
                    videoWatchingTimes: 0
                });
                userData.changed('learningRecord', true);
            }
        }

        userData.save();

    });

    AllData.findOne({
        where: {
            videoName: req.body.videoName
        }
    }).then(function (singleData) {
        console.log("singleData");
        console.log(singleData);

        if (!singleData.viewingTimes) {
            singleData.viewingTimes = 0;
        }
        if (!req.body.videoSkip) {
            singleData.viewingTimes++;
        }
        singleData.save();
    });

    WordTests.findById(req.body.testUniId).then(function (wordTest) {
        console.log(wordTest);
        console.log('uniid');
        console.log(req.body.testUniId);

        if (req.body.pass && !req.body.videoSkip) {
            console.log("非翻译题");
            if (!wordTest.allTestTimes) {
                wordTest.allTestTimes = 0
            }
            wordTest.allTestTimes++;
            if (!wordTest.allPassTimes) {
                wordTest.allPassTimes = 0
            }
            if (req.body.pass) {
                wordTest.allPassTimes++;
            }
        } else {
            console.log("翻译题");
            if (!wordTest.allTestTimes) {
                wordTest.allTestTimes = 0
            }
            wordTest.allTestTimes++;
        }
        wordTest.save();

    });
    res.send({})
});

router.post("/ranking", jsonParser, function (req, res, err) {

    var userStudiedData = [];
    var resData = {};

    function compare(a, b) {
        if (a.studied.length > b.studied.length) {
            return -1;
        } else if (a.studied.length < b.studied.length) {
            return 1;
        } else if (a.studied.length == 0 && b.studied.length == 0) {
            return 0;
        } else {
            var alastLearningTime = a.studied[a.studied.length - 1].passTime;
            var blastLearningTime = b.studied[b.studied.length - 1].passTime;
            if (alastLearningTime > blastLearningTime) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    UserData.findAll().then(function (userData) {
        if (userData) {
            userData.map(function (singleData, index) {
                var singleRankingData = {};
                if (!singleData.studied) {
                    singleData.studied = [];
                }
                singleRankingData.studied = singleData.studied;
                singleRankingData.openid = singleData.openid;
                userStudiedData.push(singleRankingData);
            })
        }
        userStudiedData.sort(compare);

        userStudiedData.map(function (userStudiedDataItem, index) {
            if (userStudiedDataItem.openid == req.session.openid) {
                resData.rankNum = index + 1;
                console.log(resData.rankNum);
            }
        });

        if (resData.rankNum == 1) {
            resData.prevDis = null;
        } else if (resData.rankNum > 1) {
            resData.prevDis = userStudiedData[resData.rankNum - 2].studied.length - userStudiedData[resData.rankNum - 1].studied.length;
        } else {
            resData.prevDis = 0;
        }
        resData.haveStudied = userStudiedData[resData.rankNum - 1].studied.length;


        // res.send({ranking: userStudiedData});
    }).then(function () {
        UserInfo.findOne({where: {openid: req.session.openid}}).then(function (userInfo) {
            resData.headImg = userInfo.headimgurl;
            resData.nikeName = userInfo.nikeName;
            console.log(resData);
            res.send({resData: resData})
        })
    })
});

router.get("*", function (req, res) {
    res.sendFile(path.resolve(__dirname, '../dist/index.html'));
});

module.exports = router;